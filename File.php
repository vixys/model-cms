<?php

namespace Model\CMS;

class File {

    use \Model\CMS\Component_datatype_behavior;

    public function datatype($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, !empty($item['alias']) ? $item['alias'] : \Crush\Basic::getClassShortName($this));
        $baseDir = __DIR__ . '/../../../';
        $results = [];
        
        empty($mySets['pattern']) && ($mySets['pattern'] = '*');
        if($mySets['path']) {
            $path = $baseDir . $mySets['path'] . (substr($mySets['path'], -1, 1)==='/' ? '' : '/');
            $list = (array) @glob($path . $mySets['pattern'], GLOB_BRACE);
            
            foreach($list as $x => $fullItemPath) {
                $itemPath = str_replace($baseDir, '/', $fullItemPath);
                $itemName = @array_pop(explode('/', $itemPath));
                
                $include = false;
                switch(true) {
                    case (string)@$mySets['type'] === 'd':
                        if(is_dir($fullItemPath)) {
                            $include = true;
                        }
                        break;
                    case (string)@$mySets['type'] === 'f':
                        if(is_file($fullItemPath)) {
                            $include = true;
                        }
                        break;
                    default:
                        $include = true;
                        break;
                }
                
                if($include) {
                    $results[] = [
                        'path' => $itemPath,
                        'name' => $itemName,
                    ];
                }
                 
           }
        }
        
        return $results;
    }

}
