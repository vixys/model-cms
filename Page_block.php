<?php

namespace Model\CMS;

class Page_block {

    use \doctrine\Dashes\Model {
        \doctrine\Dashes\Model::find as protected _find;
    }

    protected $modelAttrDefaults = [
        'table' => 'page_block',
//    protected $recursive = \HBasis\HASMANY;
//    protected $relatedRecursive = \HBasis\HASMANY;
        'foreignKeys' => [
            'page' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'page_id',
                'model' => '\Model\Page'
            ],
            'page_component' => [
                'type' => \HBasis\HASMANY,
                'key' => 'page_block_id',
                'model' => '\Model\Page_component'
            ],
        ],
    ];

    const type_component = 1;
    const type_content = 2;

    public $enum = [
    ];
    

    public function find($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        empty($orderby) && $orderby = 'order ASC';
        return $this->_find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

    public function composeAll($page, $data, $html, $l) {
        $blocksHtml = '';
        $c = 0;

        $rowFill = 0;
        foreach ($page['page_block'] as $x => $pageBlock) {
            $tpl = $this->getTemplates();
            $composeResult = $this->compose($pageBlock, $page, $data, $html, $l, $tpl);
            
            if(!empty($_GET['debughtml'])) {
                $blocksHtml .= "\n". "<!-- (open) page #".$pageBlock['page_id'] . ' -> ' . $pageBlock['name']." -->" . "\n";
            }
            
            $blocksHtml .= $composeResult;
            
            if(!empty($_GET['debughtml'])) {
                $blocksHtml .= "\n". "<!-- (close) page #".$pageBlock['page_id'] . ' -> ' . $pageBlock['name']." -->" . "\n";
            }
            
            $c++;
        }

        return $blocksHtml;
    }
    
    public function getTemplates() {
        $dynModel = $this->loadModel('\Model\Dynamic');
        $dynList = $dynModel->getList(['template','page']);

        $tpl = [
            'col' => '',
            'row' => '',
        ];
        if(count($dynList)===1 && !empty($dynList[0]['hasmany'])) {
            $tpl = \Crush\Collection::transform($dynList[0]['hasmany'], 'ukey');
        }

        return $tpl;
    }

    public function compose($pageBlock, $page, $data, $html, $l, $tpl) {
        
        $this->loadModelInstance($this->getAttr('foreignKeys')['page_component']['model']);
        // blocos sem html customizado são sempre de componentes (padrao)
        // blocos com html customizado recebem a estrutura padrao
        $pageBlock['html'] = trim($pageBlock['html']);

        $pageBlockHtmlStructure = !empty($pageBlock['tpl_col']) ? $tpl['col']['value'] : '{{content}}';
        $pageBlockHtmlStructure = str_replace('{{content}}',$pageBlockHtmlStructure,!empty($pageBlock['tpl_row']) ? $tpl['row']['value'] : '{{content}}');
        // blocos de componente -> poderão ter html customizado, mas dentro da div padrão, que contém o tamanho do bloco
        // blocos de conteudo -> poderão ter seu html completamente customizado
        !empty($pageBlock['html']) && ($pageBlockHtmlStructure = $pageBlock['html']);
        $vars = array_merge(['page'=>$data],[
            'tpl' => $tpl,
        ]);

        $result = $html;
        $scripts = [];
        $pageComponentResult = [];
        if (!empty($pageBlock['page_component'])) {
            $pageComponentResult = $this->model['Page_component']->composeAll($pageBlock, $page, $vars, $html);
            $result = $pageComponentResult['html'];
            $scripts = $pageComponentResult['_scripts'];
        }

        $vars = array_merge($vars, [
            '_scripts' => $scripts,
        ]);
        $vars['vars'] = $vars['page']['vars'];

        $pageBlockHtml = str_replace(['{{content}}'], ['##content##'], $pageBlockHtmlStructure);
        $pageBlockHtml = static::CMS_RenderHTML($pageBlockHtml, $vars);
        $pageBlockHtml = str_replace('##content##', $result, $pageBlockHtml);

        return $pageBlockHtml;
    }

}
