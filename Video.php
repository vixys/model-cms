<?php

namespace Model\CMS;

class Video {

    use \doctrine\Dashes\Model,
        \Model\CMS\Component_datatype_behavior,
        Generic_tag {
        \doctrine\Dashes\Model::getBy as protected _getBy;
    }

    protected $modelAttrDefaults = [
        'table' => 'video',
        'foreignKeys' => [
            'category' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'category_id',
                'model' => '\Model\CMS\Category'
            ],
            'tag' => [
                'type' => \HBasis\HASMANY,
                'key' => 'video_id',
                'model' => '\Model\CMS\Video_tag'
            ],
            'param' => [
                'type' => \HBasis\HASMANY,
                'key' => 'video_id',
                'model' => '\Model\CMS\Video_param'
            ],
        ],
        'fieldsFormat' => [
            'publish_date' => ['/(\d{2})\/(\d{2})\/(\d{4})\s(\d{2})\:(\d{2})/', '$3-$2-$1 $4:$5'],
            'category_id' => ':',
            'created' => ':',
            'modified' => ':',
            'link' => ':',
            'slug' => ':',
            'old_slug' => ':',
        ],
    ];

    public function format_category_id($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        return !empty($data['category_id']) ? $data['category_id'] : null;
    }

    public function format_link($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        $regExp = "/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/";
        preg_match($regExp, $value, $matches);
        $youtubeId = (!empty($matches) && count($matches) > 7 && strlen($matches[7]) == 11) ? $matches[7] : $value;
        return $youtubeId;
    }

    public function format_slug($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        if (!empty($data['title'])) {
            $slugify = new \Cocur\Slugify\Slugify();
            return $slugify->slugify($data['title']);
        }
    }

    public function addView($item) {
        is_array($item) ? ($id = $item['id']) : ($id = $item);

        if (!$this->getData($id)) {
            is_int($item) && ($item = $this->get($id));

            $updateData = [];
            $updateData['id'] = $item['id'];
            $updateData['views'] = (int) $item['views'] + 1;
            $this->update($id, $updateData);
            $this->addData($id, true);
            return true;
        }

        return false;
    }

    public function getList($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $conditions['publish_date' . ' <='] = date('Y-m-d H:i');
        $conditions['status'] = '1';

        return $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

    public function getBy($conditions = array(), $columns = null, $orderby = null, $recursive = null) {
        $conditions['publish_date <='] = date('Y-m-d H:i');
        $conditions['status'] = '1';

        $item = $this->_getBy($conditions, $columns, $orderby, $recursive);
        return $item;
    }

    public function datatype($item, $categoryId = NULL, $settings = [], $data = []) {
        $mySets = $this->_getSettings($settings, !empty($item['alias']) ? $item['alias'] : \Crush\Basic::getClassShortName($this));

        $criteria = [];


        $data = array_merge($data, $mySets);
        
        /* abstract soon. define common columns for table of contents */
        !empty($mySets['recent']) && ($mySets['recent'] = 'publish_date DESC, priority');
        if (!empty($mySets['filter'])) {
            !empty($data['filter_title']) && ($criteria['title LIKE'] = '%' . $data['filter_title'] . '%');
            if (!empty($data['filter_text'])) {
                $filtertext = $data['filter_text'];

                $filterCriteria = [];
                $filterCriteria[] = 'title LIKE "%' . $filtertext . '%"';
//                $filterCriteria[] = 'preview LIKE "%' . $filtertext . '%"';
                $filterCriteria[] = 'article LIKE "%' . $filtertext . '%"';
//                $filterCriteria[] = '(SELECT id FROM publisher WHERE id = post.publisher_id AND name LIKE "%' . $filtertext . '%" ) IS NOT NULL';
                $filterCriteria[] = '(SELECT id FROM category WHERE id = post.category_id AND title LIKE "%' . $filtertext . '%" ) IS NOT NULL';

                $criteria[] = '(' . implode(' OR ', $filterCriteria) . ')';
            }
            array_key_exists('filter_slug', $data) && 
                (!empty(trim($data['filter_slug'])) || !empty($data['filter_slug_empty'])) && 
                ($criteria['slug'] = empty($data['filter_slug']) ? ' ' : $data['filter_slug'] );
//            !empty($data['filter_publisher']) && ($criteria['publisher_id'] = $data['filter_publisher']);
            !empty($data['filter_category']) && ($criteria['category_id'] = $data['filter_category']);
            if(!empty($data['filter_category_slug'])) {
                strpos($data['filter_category_slug'], '%')!==false && ($data['filter_category_slug'] = urldecode($data['filter_category_slug']));
                $category = $this->loadFkModel('category')->getBy(['slug'=>explode('|',$data['filter_category_slug'])]);
                $data['filter_category'] = !empty($category) && !empty($category['id']) ? $category['id'] : '0';
                ($criteria['category_id'] = $data['filter_category']);
            }
        }
        

        $results = $this->execCommonFind($this, $criteria, $categoryId, $mySets);
        return $results;
    }

}
