<?php

namespace Model\CMS;

class Survey_question_answer {

    use \doctrine\Dashes\Model;

    protected $modelAttrDefaults = [
        'table' => 'survey_question_answer',
        'recursive' => \HBasis\HASMANY,
        'foreignKeys' => [
//        'question' => [
//            'type' => \HBasis\BELONGSTO,
//            'key' => 'survey_question_id',
//            'model' => '\Model\Survey_question'
//        ]
//        
//        'response' => [
//            'type' => \HBasis\HASMANY,
//            'key' => 'survey_question_answer_id',
//            'model' => '\Model\Survey_question_response'
//        ]
        ],
    ];

    public function addVote($id) {
        $answer = $this->get($id);
        $answer['votes'] = (int) $answer['votes'] + 1;
        return $this->update($id, $answer);
    }

}
