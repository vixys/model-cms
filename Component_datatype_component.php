<?php

namespace Model\CMS;

/* FEEDERS OF A COMPONENT */
class Component_datatype_component {

    use \doctrine\Dashes\Model;
    
    protected $table = 'component_datatype_component';
//    protected $recursive = \HBasis\HASMANY;
//    protected $relatedRecursive = \HBasis\HASMANY;
    public $foreignKeys = [
        'component' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'component_id',
            'model' => '\Model\Component'
        ],
        'datatype' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'component_datatype_id',
            'model' => '\Model\Component_datatype'
        ],
    ];
    protected $fieldsFormat = [
        'alias' => ':',
    ];

    public function format_alias($field, $value, $format, $data) {
        if ($value === NULL) return false; // variable not used/changed on the proccess
        
        return preg_replace("/\s+/", '', $value);
    }

}
