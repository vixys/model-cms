<?php

namespace Model\CMS;

class Product_price {

    use \doctrine\Dashes\Model {
        \doctrine\Dashes\Model::findHasMany as _findHasMany;
    }

    protected $modelAttrDefaults = [
        'table' => 'product_price',
        'deactivate' => \DB_FIELD_DELETE,
        'foreignKeys' => [
            'product' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'product_id',
                'model' => '\Model\CMS\Product'
            ],
            'param' => [
                'type' => \HBasis\HASMANY,
                'key' => 'product_price_id',
                'model' => '\Model\CMS\Product_price_param'
            ],
        ],
        'fieldsFormat' => [
//            'publish_date' => ['/(\d{2})\/(\d{2})\/(\d{4})\s(\d{2})\:(\d{2})/', '$3-$2-$1 $4:$5'],
            'price_unit' => ':',
            'price_unit_2' => ':',
            'price_total' => ':',
            'price_total_2' => ':',
//            'created' => ':',
//            'modified' => ':',
        ],
    ];
    
    public function findHasMany($results, $relatedNeededList = []) {
        $results = $this->_findHasMany($results, $relatedNeededList);
        
        if(!empty($results)) {
            foreach($results as $x => $y) {
                if(!empty($y['param'])) {
                    $results[$x]['param'] = \Crush\Collection::transform($y['param'], 'dynamic_ukey');
                }
            }
        }
        
        return $results;
    }

//    public function format_price_type_id($field, $value, $format, $data) {
//        if ($value === NULL)
//            return false; // variable not used/changed on the proccess
//
//        return !empty($data['price_type_id']) ? $data['price_type_id'] : null;
//    }
}
