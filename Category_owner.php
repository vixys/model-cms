<?php

namespace Model\CMS;

class Category_owner {

    use \doctrine\Dashes\Model;

    protected $modelAttrDefaults = [
        'table' => 'category_owner',
        'foreignKeys' => [
            'category' => [
                'type' => \HBasis\HASMANY,
                'key' => 'category_owner_id',
                'model' => '\Model\CMS\Category'
            ],
            'page' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'page_id',
                'model' => '\Model\Page'
            ]
        ],
    ];

    public function getPageId($owner = '') {
        $categoryOwner = $this->getBySlug($owner);
        $pageId = $categoryOwner['page_id'];

        return $pageId;
    }

    public function getBySlug($owner = '') {
        $conditions = [];
        (!empty($owner) && $owner !== 'x') && ($conditions['slug'] = $owner);

        $categoryOwner = $this->getBy($conditions);
        empty($categoryOwner) && ($categoryOwner = $this->getBy([]));

        return $categoryOwner;
    }

}
