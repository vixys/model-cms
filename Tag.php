<?php

namespace Model\CMS;

class Tag {

    use \doctrine\Dashes\Model {
        \doctrine\Dashes\Model::save as protected _save;
    }

    protected $modelAttrDefaults = [
        'table' => 'tag',
        'foreignKeys' => [
            'video_tag' => [
                'type' => \HBasis\HASMANY,
                'key' => 'tag_id',
                'model' => '\Model\CMS\Video_tag'
            ],
            'post_tag' => [
                'type' => \HBasis\HASMANY,
                'key' => 'tag_id',
                'model' => '\Model\CMS\Post_tag'
            ],
            'publisher_tag' => [
                'type' => \HBasis\HASMANY,
                'key' => 'tag_id',
                'model' => '\Model\CMS\Publisher_tag'
            ],
            'service_tag' => [
                'type' => \HBasis\HASMANY,
                'key' => 'tag_id',
                'model' => '\Model\CMS\Service_tag'
            ],
            'product_tag' => [
                'type' => \HBasis\HASMANY,
                'key' => 'tag_id',
                'model' => '\Model\CMS\Product_tag'
            ],
        ],
    ];

    public function save($data) {
        if (empty($data['id'])) {
            /* avoid tag duplication */
            $tag = $this->getBy($data);
            if (!empty($tag)) {
                return $tag['id'];
            }
        }

        return $this->_save($data);
    }

    public function identifyTags($value) {
        is_array($value) && count($value) === 1 && is_string($value[0]) && ($value = $value[0]);
        if (is_string($value)) {
            list($delimiter, $otherDelimiter) = strpos($value, ';') !== false ? [';', ','] : [',', ';'];
            $tagList = explode($delimiter, str_replace($otherDelimiter, $delimiter, $value));
            foreach ($tagList as $x => $tagItem) {
                $tagList[$x] = trim($tagItem);
                if (empty($tagList[$x])) {
                    unset($tagList[$x]);
                }
            }

            $value = $tagList;
        }

        return $value;
    }

}
