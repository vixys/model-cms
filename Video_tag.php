<?php

namespace Model\CMS;

class Video_tag extends Generic_bridge_tag {

    protected $modelAttrDefaults = [
        'table' => 'video_tag',
        'foreignKeys' => [
            'video' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'video_id',
                'model' => '\Model\CMS\Video'
            ],
            'tag' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'tag_id',
                'model' => '\Model\CMS\Tag'
            ],
        ],
    ];

}
