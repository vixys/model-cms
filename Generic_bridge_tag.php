<?php

namespace Model\CMS;

/**
 * Common behavior to persist tags into a bridge table between tag table and the owners
 */
class Generic_bridge_tag {

    use \doctrine\Dashes\Model {
        \doctrine\Dashes\Model::create as protected _create;
    }

    public function create($data) {
        if (empty($data)) {
            return false;
        }

        $data = $this->saveBelongsTo($data);

        if (empty($data['id'])) {
            $results = $this->findAll($data, null, null, null, null, \HBasis\NORELATED);
            $bridgeTag = count($results) === 1 ? $results[0] : [];
            if (!empty($bridgeTag)) {
                /* reutiliza tags que foram removidas anteriormente */
                if ((string) $bridgeTag[$this->getAttr('deactivate')] === (string)$this->getAttr('deactivateValue')) {
                    $bridgeTag[$this->getAttr('deactivate')] = $this->getAttr('activateValue');
                    $this->update($bridgeTag['id'], $bridgeTag);
                }
                return $bridgeTag['id'];
            }
        }

        return $this->_create($data);
    }

}
