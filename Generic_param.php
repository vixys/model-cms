<?php

namespace Model\CMS;

class Generic_param {

    use \doctrine\Dashes\Model;

    public function getDynFk($ukey) {
        $paramParentUkey = $this->getAttr('param_dyn_ukey');
        if(empty($ukey) || empty($paramParentUkey)) {
            throw new \Exception('<Missing param>');
        }
        
        $dynModel = $this->loadModel('\Model\Dynamic');
        $dynServiceParam = $dynModel->getList([$ukey,$paramParentUkey.'.param']);
        
        return count($dynServiceParam) > 0 ? $dynServiceParam[0] : null;
    }

    public function getDynFkList() {
        $paramParentUkey = $this->getAttr('param_dyn_ukey');
        if(empty($paramParentUkey)) {
            throw new \Exception('<Missing param>');
        }
        $dynModel = $this->loadModel('\Model\Dynamic');
        $dynServiceParam = $dynModel->getList(['param',$paramParentUkey]);
        $serviceParamList = !empty($dynServiceParam[0]) ? (array) @$dynServiceParam[0]['hasmany'] : [];
        $serviceParamList = \Crush\Collection::transform($serviceParamList, 'ukey', []);
        return $serviceParamList;
    }
}
