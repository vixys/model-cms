<?php

namespace Model\CMS;

class Service_tag extends Generic_bridge_tag {

    protected $modelAttrDefaults = [
        'table' => 'service_tag',
        'foreignKeys' => [
            'service' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'service_id',
                'model' => '\Model\CMS\Service'
            ],
            'tag' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'tag_id',
                'model' => '\Model\CMS\Tag'
            ],
        ],
    ];

}
