<?php

namespace Model\CMS;

class Category {
    
    use \doctrine\Dashes\Model,
        \Model\CMS\Component_datatype_behavior;

    protected $modelAttrDefaults = [
        'table' => 'category',
//    protected $recursive = \HBasis\HASMANY;    
//    protected $relatedRecursive = \HBasis\HASMANY;
        'foreignKeys' => [
            'parent' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'category_id',
                'model' => '\Model\CMS\Category'
            ],
            'owner' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'category_owner_id',
                'model' => '\Model\CMS\Category_owner'
            ],
            'param' => [
                'type' => \HBasis\HASMANY,
                'key' => 'category_id',
                'model' => '\Model\CMS\Category_param'
            ],
        ],
        'fieldsFormat' => [
            'created' => ':',
            'modified' => ':',
            'category_id' => ':',
            'level' => ':',
            'slug' => ':',
            'old_slug' => ':',
        ],
    ];


    public function format_level($field, $value, $format, $data) {
        if ($value === NULL) return false; // variable not used/changed on the proccess
        if(empty($data['category_id'])) return 1;
        
        $parentLevel = (int)$this->get($data['category_id'])['level'];
        (!$parentLevel) && ($parentLevel = 1);
        return $parentLevel + 1;
    }

    public function format_category_id($field, $value, $format, $data) {
        if ($value === NULL) return false; // variable not used/changed on the proccess
        
        return !empty($data['category_id']) ? $data['category_id'] : null;
    }

    public function findTree($conditions = [], $order = '') {
        empty($order) && ($order = 'id ASC');
        
        $list = $this->find($conditions, NULL, NULL, NULL, $order, -1);
        return (array) @\Crush\Collection::array_tree_to_single(\Crush\Collection::convertToTree($list, 'id', 'category_id'));
    }

    public function getTree($conditions = [], $order = '') {
        $list = $this->findTree($conditions, $order);

        foreach ($list as $x => $item) {
            $item['title2'] = $item['title'];
            $item['level'] = (int) $item['level'];
            if ($item['level'] > 1) {
                $item['title2'] = str_pad('', (int) $item['level'] - 1, '-', STR_PAD_LEFT) . ' ' . $item['title'];
            }

            $list[$x] = $item;
        }

        return $list;
    }
    
    public function __call($name, $arguments) {
        switch(true) {
            case strpos($name, 'listpermodule_')===0:
                $info = explode('_', $name, 2);
                return $this->{$info[0]}($info[1]);
                break;
        }
    }
    
    public function listpermodule($alias) {
        $dynModel = $this->loadModel('\Model\Dynamic');
        $dynCat = $dynModel->getItem(['parent_category',$alias]);
//        $catConditions = !empty($dynCat) && !empty($dynCat['value'])  ? ['category_id'=>(int)@$dynCat['value']] : [];
        $categoryList = \Crush\Collection::transform($this->getTree([], 'category_owner_id ASC, title ASC'), 'id');
        
        $treeList = \Crush\Collection::convertToTree($categoryList, 'id', 'category_id');
        if(!empty($dynCat) && !empty($dynCat['value']) && !empty($treeList[$dynCat['value']])) {
            $treeList = $treeList[$dynCat['value']]['children'];
        }
        $categoryList = \Crush\Collection::transform(\Crush\Collection::array_tree_to_single($treeList), 'id');
        return $categoryList;
    }

    public function datatype($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, !empty($item['alias']) ? $item['alias'] : \Crush\Basic::getClassShortName($this));

        $criteria = $this->buildCommonCriteria([], $categoryId, $mySets);
        $order = $this->buildCommonOrder($criteria, $categoryId, $mySets);
        unset($criteria['status']);
        
        $results = $this->applyCommonFilters($this->getTree($criteria, $order), $mySets);
        return $results;
    }

}
