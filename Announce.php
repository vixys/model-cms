<?php

namespace Model\CMS;

class Announce {

    use \doctrine\Dashes\Model,
        \DataBoomer\Session,
        \Model\CMS\Component_datatype_behavior,
        Generic_tag {
        \doctrine\Dashes\Model::getBy as protected _getBy;
    }

    protected $modelAttrDefaults = [
        'table' => 'announce',
        'foreignKeys' => [
            'category' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'category_id',
                'model' => '\Model\CMS\Category'
            ],
            'tag' => [
                'type' => \HBasis\HASMANY,
                'key' => 'announce_id',
                'model' => '\Model\CMS\Announce_tag'
            ],
            'param' => [
                'type' => \HBasis\HASMANY,
                'key' => 'announce_id',
                'model' => '\Model\CMS\Announce_param'
            ],
        ],
        'fieldsFormat' => [
            'pic_small' => ':',
            'pic_large' => ':',
            'start' => ':',
            'end' => ':',
            'category_id' => ':',
            'created' => ':',
            'created_by' => ':',
            'modified' => ':',
            'modified_by' => ':',
        ],
    ];

    const type_img = 1;
    const type_html = 2;
    const format_square = 1;
    const format_vert_ret = 2;
    const format_ret = 3;
    const format_sup = 4;
    const format_full = 5;
    const format_half = 6;
    const format_sky = 7;

//    const format_lightbox = 8;

    public $enum = [
        'type' => [
            self::type_img => 'Imagem',
            self::type_html => 'HTML',
        ],
        'format' => [
            self::format_square => 'SQUARE',
            self::format_vert_ret => 'VERTICAL RECTANGLE',
            self::format_ret => 'RECTANGLE',
            self::format_sup => 'SUPERBANNER',
            self::format_full => 'FULL BANNER',
            self::format_half => 'HALF BANNER',
            self::format_sky => 'SKYSCRAPER',
//            self::format_lightbox => 'LIGHTBOX',
        ],
    ];

    public function format_pic_small($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        if (empty($value)) {
            return NULL;
        }
        return preg_replace("/^\//", '', $value);
    }

    public function format_pic_large($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        if (empty($value)) {
            return NULL;
        }
        return preg_replace("/^\//", '', $value);
    }

    public function format_start($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        if (empty($value)) {
            return NULL;
        }
        return implode('-', array_reverse(explode('/', $value)));
    }

    public function format_end($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        if (empty($value)) {
            return NULL;
        }
        return implode('-', array_reverse(explode('/', $value)));
    }

    public function format_category_id($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        return !empty($data['category_id']) ? $data['category_id'] : null;
    }

    public function getList($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $conditions['start' . ' <='] = date('Y-m-d');
        $conditions[] = '(end IS NULL OR end >= \'' . date('Y-m-d') . '\')';
        $conditions['status'] = '1';

        $list = $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
        foreach ($list as $x => $y) {
            $list[$x] = $this->applyCounterRoute($y);
        }
        return $list;
    }

    public function getBy($conditions = array(), $columns = null, $orderby = null, $recursive = null) {
        $conditions['start' . ' <='] = date('Y-m-d');
        $conditions[] = '(end IS NULL OR end >= \'' . date('Y-m-d') . '\')';
        $conditions['status'] = '1';

        $item = $this->applyCounterRoute($this->_getBy($conditions, $columns, $orderby, $recursive));
        return $item;
    }

    public function applyCounterRoute($item) {
        $item['_link'] = base_url() . 'anuncio/' . $item['id'];
        return $item;
    }

    /**
     * Keeps the number of visits
     */
    public function addClick($item) {
        is_array($item) ? ($id = $item['id']) : ($id = $item);
        !is_array($item) && ($item = $this->get($id));

        if (empty(@$_SERVER['REQUEST_TRUST']) && !$this->getData('click#' . $id)) {
            $nitem = ['id' => $item['id']];
            $nitem['clicks'] = (int) $item['clicks'] + 1;
            $this->update($id, $nitem);
            $this->addData('click#' . $id, true);
        }

        return $item;
    }

    /**
     * Keeps the number of views
     */
    public function addView($item) {
        is_array($item) ? ($id = $item['id']) : ($id = $item);
        is_int($item) && ($item = $this->get($id));

        if (empty(@$_SERVER['REQUEST_TRUST']) && !$this->getData('view#' . $id)) {
            $nitem = ['id' => $item['id']];
            $nitem['views'] = (int) $item['views'] + 1;
            $this->update($id, $nitem);
            $this->addData('view#' . $id, true);
            return true;
        }

        return false;
    }

    /**
     * Keeps the number of prints
     */
    public function addPrint($item, $hash) {
        is_array($item) ? ($id = $item['id']) : ($id = $item);
        is_int($item) && ($item = $this->get($id));

        if (empty(@$_SERVER['REQUEST_TRUST']) && $this->getData('print#' . $hash . '#' . $id) === 1) {
            $nitem = ['id' => $item['id']];
            $nitem['prints'] = (int) $item['prints'] + 1;
            $this->update($id, $nitem);
            $this->addData('print#' . $hash . '#' . $id, 2);
            return true;
        }

//        return false;
    }

    /* store the key identification to allow print to be added */
    /* this will solve any problems for people that try to mess up the count */

    public function preparePrint($item, $hash) {
        is_array($item) ? ($id = $item['id']) : ($id = $item);

        empty(@$_SERVER['REQUEST_TRUST']) && ($this->addData('print#' . $hash . '#' . $id, 1));
    }

    public function datatype_square($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, \Crush\Basic::getClassShortName($this).'_square');

        $criteria = [];

        $criteria['format'] = static::format_square;

        return $this->datatype($criteria, $item, $categoryId, $mySets);
    }

    public function datatype_full($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, \Crush\Basic::getClassShortName($this).'_full');

        $criteria = [];

        $criteria['format'] = static::format_full;

        return $this->datatype($criteria, $item, $categoryId, $mySets);
    }

    public function datatype_half($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, \Crush\Basic::getClassShortName($this).'_half');

        $criteria = [];

        $criteria['format'] = static::format_half;

        return $this->datatype($criteria, $item, $categoryId, $mySets);
    }

    public function datatype_lightbox($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, \Crush\Basic::getClassShortName($this).'_lightbox');

        $criteria = [];

        $criteria['format'] = static::format_lightbox;

        return $this->datatype($criteria, $item, $categoryId, $mySets);
    }

    public function datatype_ret($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, \Crush\Basic::getClassShortName($this).'_ret');

        $criteria = [];

        $criteria['format'] = static::format_ret;

        return $this->datatype($criteria, $item, $categoryId, $mySets);
    }

    public function datatype_sky($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, \Crush\Basic::getClassShortName($this).'_sky');

        $criteria = [];

        $criteria['format'] = static::format_sky;

        return $this->datatype($criteria, $item, $categoryId, $mySets);
    }

    public function datatype_sup($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, \Crush\Basic::getClassShortName($this).'_sup');

        $criteria = [];

        $criteria['format'] = static::format_sup;

        return $this->datatype($criteria, $item, $categoryId, $mySets);
    }

    public function datatype_vert_ret($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, \Crush\Basic::getClassShortName($this).'_vert_ret');

        $criteria = [];

        $criteria['format'] = static::format_vert_ret;

        return $this->datatype($criteria, $item, $categoryId, $mySets);
    }

    public function datatype($item, $categoryId = NULL, $settings = [], $data = []) {
        $mySets = $this->_getSettings($settings, !empty($item['alias']) ? $item['alias'] : \Crush\Basic::getClassShortName($this));
        
        !array_key_exists('random', $mySets) && ($mySets['random'] = 1);
        !array_key_exists('limit', $mySets) && ($mySets['limit'] = 1);

        $results = $this->execCommonFind($this, $criteria, $categoryId, $mySets);

        foreach ($results as $x => $item) {
            $hash = urlencode(md5(uniqid('', true)));

            $this->addView($item);
            $this->preparePrint($item, $hash);

            $item['_hash'] = $hash;
            $results[$x] = $item;
        }
        return $results;
    }

}
