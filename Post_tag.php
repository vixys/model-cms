<?php

namespace Model\CMS;

class Post_tag extends Generic_bridge_tag {

    protected $modelAttrDefaults = [
        'table' => 'post_tag',
        'foreignKeys' => [
            'post' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'post_id',
                'model' => '\Model\CMS\Post'
            ],
            'tag' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'tag_id',
                'model' => '\Model\CMS\Tag'
            ],
        ],
    ];

}
