<?php

namespace Model\CMS;

class Video_param extends \Model\CMS\Generic_param {

    protected $modelAttrDefaults = [
        'table' => 'video_param',
        'foreignKeys' => [
        ],
        'param_dyn_ukey' => 'video',
    ];
}
