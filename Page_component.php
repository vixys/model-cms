<?php

namespace Model\CMS;

class Page_component {

    use \doctrine\Dashes\Model {
        \doctrine\Dashes\Model::find as protected _find;
    }

    protected $modelAttrDefaults = [
        'table' => 'page_component',
        'foreignKeys' => [
            'page_block' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'page_block_id',
                'model' => '\Model\CMS\Page_block'
            ],
            'component' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'component_id',
                'model' => '\Model\CMS\Component'
            ],
            'category' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'category_id',
                'model' => '\Model\CMS\Category'
            ],
            'page_component_settings' => [
                'type' => \HBasis\HASMANY,
                'key' => 'page_component_id',
                'model' => '\Model\CMS\Page_component_settings'
            ],
        ],
        'fieldsFormat' => [
            'category_id' => ':',
        ],
    ];

    /** used to store data of multiple fields into one without broken it's data */
    const splitCombo = ',@#$';

    public function find($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        empty($orderby) && $orderby = 'order ASC';
        return $this->_find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

    /**
     * Created to organize multiple values for the same setting
     * used by prepareRelatedData_page_component_settings and extractSettings
     * @var type 
     */
    protected $pageComponentSettingCounter = [];

    public function prepareRelatedData_page_component_settings($foreignData, $data, $foreignConfig) {
        /* desativa todas settings para depois ativar somente as enviadas pelo usuario */
        $this->deactivateAllSettings($data['id']);

//        $settingsList = !empty($foreignData) ? explode(self::splitCombo, array_pop($foreignData)) : [];
        $settingsList = preg_split('/' . preg_quote(self::splitCombo) . '/', array_pop($foreignData));
//        printf('<pre>%s</pre>', var_export($settingsList, true));die;
        if (!empty($settingsList)) {
            $foreignData = [];
            foreach ($settingsList as $x) {
                if (empty($x))
                    continue;
                list($id, $value) = preg_split('/' . preg_quote('=') . '/', $x, 2);

                empty($this->pageComponentSettingCounter[$id]) && ($this->pageComponentSettingCounter[$id] = 0);

                $componentSetting = [
                    'component_settings_id' => $id,
                    'value' => $value,
                    '_counter' => $this->pageComponentSettingCounter[$id] ++,
                ];

                $foreignData[] = $componentSetting;
            }
        }

        return $foreignData;
    }

    public function deactivateAllSettings($id) {
        $this->loadModelInstance($this->getAttr('foreignKeys')['page_component_settings']['model']);
        $this->model['Page_component_settings']->updateBy(['page_component_id' => $id], [$this->getAttr('deactivate') => $this->getAttr('deactivateValue')]);
    }

    public function format_category_id($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        return !empty($data['category_id']) ? $data['category_id'] : null;
    }

    public function composeAll($block, $page, $data, $html) {
        $this->loadModelInstance($this->getAttr('foreignKeys')['component']['model']);

        $pageComponentHtml = '';
        $pageComponentScripts = [];
        if (!empty($block['page_component'])) {
            foreach ($block['page_component'] as $x => $pageComponent) {
                $composeResult = $this->compose($pageComponent, $block, $page, $data, $html);
                if (is_string(@$composeResult['html'])) {
                    if(!empty($_GET['debughtml'])) {
                        $pageComponentHtml .= "\n". "<!-- (open) page #".$page['id'] . ' -> component #' . $pageComponent['component']['id'] ." -->" . "\n";
                    }
                    
                    $pageComponentHtml .= $composeResult['html'];
                    
                    if(!empty($_GET['debughtml'])) {
                        $pageComponentHtml .= "\n". "<!-- (close) page #".$page['id'] . ' -> component #' . $pageComponent['component']['id'] ." -->" . "\n";
                    }
                    
                    $pageComponentScripts[$pageComponent['id']] = $composeResult['_scripts'];
                }
            }
            return ['html'=>$pageComponentHtml, '_scripts'=>$pageComponentScripts];
        }
    }

    public function compose($pageComponent, $block, $page, $data, $html) {
        $settings = [];
        $settings['uniqid'] = $pageComponent['id'];
        $settings['_cat_color'] = '';

        $pageComponentSettings = $this->extractSettings((array) @$pageComponent['page_component_settings'], $data['page']);
        $settings = array_merge($settings, $pageComponentSettings);

        $componentCompose = $this->model['Component']->compose($pageComponent['component'], null, $settings, $data);

        return $componentCompose;
    }

    /**
     * Utility that select for the user the category located on the first feeder to set the css of .cat_color
     * @param type $setsList
     * @return integer
     */
    public function _getFirstCatOnSets($setsList) {
        foreach ($setsList as $datatype => $sets) {
            if (!empty($sets['category'])) {
                return $sets['category'];
            }
        }
    }

    /**
     * 
     * @param type $pageComponentSettings
     * @param type $data global data of the page thats been loaded
     * @return array
     */
    public function extractSettings($pageComponentSettings, $pageData) {
        $componentSettingsModel = $this->loadModelInstance($this->model['Component']->getAttr('foreignKeys')['component_settings']['model']);
        
        /* strpad to avoid conflict on ordering keys */
        foreach($pageComponentSettings as $x => $y) {
            $y['component_settings']['group11'] = str_pad($y['component_settings']['group'], 11, '0', STR_PAD_LEFT);
            $y['component_settings']['order11'] = str_pad($y['component_settings']['order'], 11, '0', STR_PAD_LEFT);
            $y['component_settings']['id11'] = str_pad($y['component_settings']['id'], 11, '0', STR_PAD_LEFT);
            $y['id11'] = str_pad($y['id'], 11, '0', STR_PAD_LEFT);
            
            $pageComponentSettings[$x] = $y;
        }
        /**/
        
        $settings = \Crush\Collection::transform($pageComponentSettings, 'component_settings.group11+.+component_settings.order11+.+component_settings.id11+.+id11', ['value', 'component_settings.id', 'component_settings.entry_name entry_name', 'component_settings.entry_type entry_type', 'component_settings.default_value default']);
        ksort($settings);

        $results = [];
        $test = [];
        $c = 0;
        foreach ($settings as $index => $data) {
            if((int) $data['entry_type'] === $componentSettingsModel->getEntryItemInfoByUkey('const', 'order')) {
                continue;
            }
            
            $name = $data['entry_name'];
            $c++;
            $duplicateSetting = NULL;
            array_key_exists($name, $results) && ($duplicateSetting = $results[$name]);

            // constant value over customization
            if ((int) $data['entry_type'] === $componentSettingsModel->getEntryItemInfoByUkey('const', 'order')) {
                $result = $data['default'];
            } else {
                $result = $data['value'];
            }

            if (strpos($result, ':') !== false) {
                list($entryType, $entryConfig) = explode(':', $result, 2);
                $value = $this->model['Component']->getEntryListData($entryType, $entryConfig, $pageData);

                if ($value !== false) {
                    $result = $value;
                }
            }

            empty($this->pageComponentSettingCounter[$data['component_settings.id']]) && ($this->pageComponentSettingCounter[$data['component_settings.id']] = 0);

            if (strpos($name, '...') !== false) { // dynamic lists
                empty($this->pageComponentSettingCounter[$name]) && ($this->pageComponentSettingCounter[$name] = 1);
                $name = str_replace('...', '.' . ($this->pageComponentSettingCounter[$name] ++) . '.', $name);
            }

            if (strpos($name, '[]') !== false) { // multiple values to one setting
                empty($this->pageComponentSettingCounter[$data['component_settings.id']]) && ($this->pageComponentSettingCounter[$data['component_settings.id']] = 1);
                $name = str_replace('[]', $this->pageComponentSettingCounter[$data['component_settings.id']] ++, $name);
            }

            $results[$name] = $result;

            // if there is two parameters with the same name, it considers just the one thats not empty
            if (!empty($duplicateSetting) && empty($result)) {
                $results[$name] = $duplicateSetting;
            }
        }

        return $results;
    }

    public function generateStyle($pageComponent, $settings, $categoryId) {
        $this->loadModelInstance($this->getAttr('foreignKeys')['category']['model']);
        $category = $this->model['Category']->get($categoryId);

        $css = '';
        $componentId = 'component' . $settings['uniqid'];

        $cssFile = APPPATH . '../public/css/component-category-color.css';
        if (is_file($cssFile) && !empty($category)) {
            $color_sec = !empty($category['color_sec']) ? '#' . $category['color_sec'] : 'inherit';

            $css = '<style type="text/css">' .
                    str_replace('{{cat_color_sec}}', $color_sec, str_replace('{{cat_color}}', $category['color'], str_replace('.cat_color', '.cat_color#' . $componentId, file_get_contents($cssFile)
                                    )
                            )
                    ) .
                    '</style>';
        }

        return $css;
    }

}
