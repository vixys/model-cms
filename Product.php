<?php

namespace Model\CMS;

class Product {
    
    use \doctrine\Dashes\Model,
        Generic_tag,
        \Model\CMS\Component_datatype_behavior;

    protected $modelAttrDefaults = [
        'table' => 'product',
        'deactivate' => \DB_FIELD_DELETE,
//    protected $recursive = \HBasis\HASMANY;    
//    protected $relatedRecursive = \HBasis\HASMANY;
        'foreignKeys' => [
            'category' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'category_id',
                'model' => '\Model\CMS\Category'
            ],
            'price' => [
                'type' => \HBasis\HASMANY,
                'key' => 'product_id',
                'model' => '\Model\CMS\Product_price',
                'recursive' => \HBasis\HASMANY
            ],
            'tag' => [
                'type' => \HBasis\HASMANY,
                'key' => 'product_id',
                'model' => '\Model\CMS\Product_tag'
            ],
            'param' => [
                'type' => \HBasis\HASMANY,
                'key' => 'product_id',
                'model' => '\Model\CMS\Product_param'
            ],
        ],
        'fieldsFormat' => [
            'category_id' => ':',
        ],
    ];

    public function format_category_id($field, $value, $format, $data) {
        if ($value === NULL) return false; // variable not used/changed on the proccess
        
        return !empty($data['category_id']) ? $data['category_id'] : null;
    }

    public function getList($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $conditions['status'] = '1';

        return $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

    public $datatypeIdList = [];

    public function datatype($item, $categoryId = NULL, $settings = [], $data = []) {
        $mySets = $this->_getSettings($settings, !empty($item['alias']) ? $item['alias'] : \Crush\Basic::getClassShortName($this));
        
        $criteria = [];


        !empty($mySets['hide_duplicates']) && !empty($this->datatypeIdList) && ($criteria['id NOT'] = $this->datatypeIdList);

        $data = array_merge($data, $mySets);
        $results = [];
        $results = ($this->execCommonFind($this, $criteria, $categoryId, $mySets));
        $this->datatypeIdList = array_merge($this->datatypeIdList, \Crush\Collection::transform($results, '', ['id'], ['flatten']));

        if(!empty($mySets['get_price']) && !empty($results)) {
            $this->loadFkModel('price');
            $this->model['Product_price']->explicitAttr('foreignKeys');
            unset($this->model['Product_price']->foreignKeys['product']);
            $results = $this->findHasMany($results, ['price']);
        }
        
        if (!empty($_GET['debug']) && !empty($mySets['tagsId'])) {
            printf('<pre>%s</pre>', var_export($mySets, true));
            printf('<pre>%s</pre>', var_export($this->db, true));
            die;
        }
        
        return $results;
    }

}
