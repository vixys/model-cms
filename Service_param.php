<?php

namespace Model\CMS;

class Service_param extends \Model\CMS\Generic_param {

    protected $modelAttrDefaults = [
        'table' => 'service_param',
        'foreignKeys' => [
        ],
        'param_dyn_ukey' => 'service',
    ];
}
