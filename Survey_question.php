<?php

namespace Model\CMS;

class Survey_question {

    use \doctrine\Dashes\Model;

    protected $modelAttrDefaults = [
        'table' => 'survey_question',
        'recursive' => \HBasis\HASMANY,
        'foreignKeys' => [
            'answer' => [
                'type' => \HBasis\HASMANY,
                'key' => 'survey_question_id',
                'model' => '\Model\Survey_question_answer'
            ],
            'survey' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'survey_id',
                'model' => '\Model\Survey'
            ]
        ],
    ];

    public function vote($id, $answerId) {
        if (!$this->getData($id)) {
            $this->loadModelInstance($this->getAttr('foreignKeys')['answer']['model']);

            $this->model['Survey_question_answer']->addVote($answerId);
            $this->addData($id, $answerId);
            return true;
        }

        return false;
    }

}
