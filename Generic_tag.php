<?php

namespace Model\CMS;

trait Generic_tag {

    public function deactivateAll($id) {
        $bridgeTag = $this->loadModelInstance($this->getAttr('foreignKeys')['tag']['model']);
        $bridgeTag->updateBy([$this->getAttr('table') . '_id' => $id], [$this->getAttr('deactivate') => $this->getAttr('deactivateValue')]);
    }
    
    public function getTagList($item) {
        $origRelatedRecursive = @$this->relatedRecursive;
        $this->relatedRecursive = \HBasis\BELONGSTO;

        $bridgeTagModel = $this->loadModelInstance($this->getAttr('foreignKeys')['tag']['model']);
        $bridgeTagModel->explicitAttr('foreignKeys');
        $bridgeTagModel->foreignKeys[$this->getAttr('table')] = NULL;

        empty($item['tag']) && ($item = $this->getRelated($item, \HBasis\HASMANY, ['tag']));
        $item['tagList'] = implode(', ', \Crush\Collection::transform($item['tag'], '', ['tag.description'], ['flatten']));
        $item['tagIdList'] = implode(',', \Crush\Collection::transform($item['tag'], '', ['tag.id'], ['flatten']));
        $this->relatedRecursive = $origRelatedRecursive;

        return $item;
    }

    public function prepareRelatedData_tag($foreignData, $data, $foreignConfig) {
        $bridgeTagModel = $this->loadModelInstance($this->getAttr('foreignKeys')['tag']['model']);
        $this->loadModelInstance($bridgeTagModel->getAttr('foreignKeys')['tag']['model']);
        if (is_array($foreignData)) {
            $foreignData = $this->model['Tag']->identifyTags($foreignData);
            /* desativa todas tags do post para depois ativar somente as enviadas pelo usuario */
            $this->deactivateAll($data['id']);
            
            $relatedFk = $this->getAttr('table') . '_id';
            foreach ($foreignData as $x => $tag) {
                if (is_string($tag)) {
                    $postTag = [
                        $relatedFk => $data['id'],
                        'tag' => [
                            'description' => $tag
                        ]
                    ];

                    $foreignData[$x] = $postTag;
                }
            }
        }

        return $foreignData;
    }
}
