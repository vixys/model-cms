<?php

namespace Model\CMS;

class Publisher {

    use \doctrine\Dashes\Model,
        \Model\CMS\Component_datatype_behavior,
        Generic_tag {
        \doctrine\Dashes\Model::find as protected _find;
    }

    protected $modelAttrDefaults = [
        'table' => 'publisher',
        'foreignKeys' => [
            'post' => [
                'type' => \HBasis\HASMANY,
                'key' => 'publisher_id',
                'model' => '\Model\Post'
            ],
            'tag' => [
                'type' => \HBasis\HASMANY,
                'key' => 'publisher_id',
                'model' => '\Model\CMS\Publisher_tag'
            ],
            'param' => [
                'type' => \HBasis\HASMANY,
                'key' => 'publisher_id',
                'model' => '\Model\CMS\Publisher_param'
            ],
        ],
        'fieldsFormat' => [
            'slug' => ':',
        ],
    ];

    public function getPicName($id) {
        if (!empty($id)) {
            $filenamePiece = static::picFolder . $id . '.';
            $pic = glob($filenamePiece . '*');
            if (count($pic) === 1) {
                return $pic[0];
            }
        }
    }

    public function getList($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        unset($conditions['category_id']);
        return $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

    public function find($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $results = $this->_find($conditions, $limit, $page, $columns, $orderby, $recursive);
        if (!empty($results)) {
            foreach ($results as $x => $publisher) {
                $results[$x]['pic'] = $this->getPicName($publisher['id']);
            }
        }

        return $results;
    }

    public function getLastPosts($results) {
        $results = \Crush\Collection::transform($results, 'id');
        $idList = array_keys($results);

        $deactivateCriteria = $this->getAttr('deactivate') ? (' AND ' . $this->getAttr('deactivate') . ' <> \''.$this->getAttr('deactivateValue').'\' ') : '';
        $sql = '
            SELECT MAX(id) \'max_id\', p.* FROM
                (SELECT id, publisher_id, title, slug, category_id FROM post
                WHERE 
                    publisher_id IN (SELECT id FROM publisher WHERE id IN (' . implode(',', $idList) . '))
                    AND type = \'0\' AND status = \'1\' '.$deactivateCriteria.'
                ORDER BY 
                publisher_id ASC,
                id DESC) p
            GROUP BY publisher_id
                ';

        $query = $this->query($sql);

        $this->loadModelInstance($this->getAttr('foreignKeys')['post']['model']);
        $this->model['Post']->explicitAttr('foreignKeys');
        $fkPublisher = $this->model['Post']->foreignKeys['publisher'];
        unset($this->model['Post']->foreignKeys['publisher']);

        $posts = [];
        if(!empty($query)) {
            $posts = method_exists($query, 'result_array') ? $query->result_array() : $query->fetchAll();
            
        }
        $lastestPostList = $this->model['Post']->prepare_list($this->model['Post']->findRelated($posts));
        foreach ($lastestPostList as $lastestPost) {
            $results[$lastestPost['publisher_id']]['_last_post'] = $lastestPost;
        }

        $this->model['Post']->foreignKeys['publisher'] = $fkPublisher;

        return $results;
    }

    public function datatype($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, !empty($item['alias']) ? $item['alias'] : \Crush\Basic::getClassShortName($this));

        $criteria = [];
        !empty($mySets['display']) && $criteria['display'] = 1;

        $results = $this->execCommonFind($this, $criteria, $categoryId, $mySets);

        if (!empty($settings['last_post'])) {
            $results = $this->getLastPosts($results);
//            foreach($results as $x => $y) {
//                $results[$x]['_last_post'] = $this->prepare_post([$y['_last_post']])[0];
//            }
        }

        return $results;
    }

}
