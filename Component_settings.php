<?php

namespace Model\CMS;

class Component_settings {

    use \doctrine\Dashes\Model;

    protected $modelAttrDefaults = [
        'table' => 'component_settings',
//    protected $recursive = \HBasis\HASMANY;
//    protected $relatedRecursive = \HBasis\HASMANY;
        'foreignKeys' => [
            'component' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'component_id',
                'model' => '\Model\Component'
            ],
            'page_component_settings' => [
                'type' => \HBasis\HASMANY,
                'key' => 'component_settings_id',
                'model' => '\Model\Page_component_settings'
            ],
        ],
    ];

    protected static $entryTypeList = [];
    
    public function getEntryTypeList() {
        $dynModel = $this->loadModel('\Model\Dynamic');
        
        if(empty(static::$entryTypeList)) {
            $list = $dynModel->getList(['entry_type', 'settings'])[0]['hasmany'];
            static::$entryTypeList = \Crush\Collection::transform($list, 'ukey');
        }
        
        return static::$entryTypeList;
    }
    
    public function getIntIndexedEntryTypeList() {
        $entryTypeList = $this->getEntryTypeList();
        $list = [];
        foreach($entryTypeList as $item) {
            $list[(int) $item['order']] = $item['name'];
        }
        
        return $list;
    }
    
    public function getEntryItemInfoByUkey($ukey, $field = 'name') {
        $entryTypeList = $this->getEntryTypeList();
        return $entryTypeList[$ukey][$field];
    }

}
