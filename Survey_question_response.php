<?php

namespace Model\CMS;

class Survey_question_response {

    use \doctrine\Dashes\Model;

    protected $modelAttrDefaults = [
        'table' => 'survey_question_response',
        'foreignKeys' => [
            'answer' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'survey_question_answer_id',
                'model' => '\Model\Survey_question_answer'
            ],
            'question' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'survey_question_id',
                'model' => '\Model\Survey_question'
            ],
        ],
        'fieldsFormat' => [
            'created_by_ip' => ':',
        ],
    ];

}
