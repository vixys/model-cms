<?php

namespace Model\CMS;

class Product_price_param extends \Model\CMS\Generic_param {

    protected $modelAttrDefaults = [
        'table' => 'product_price_param',
        'foreignKeys' => [
        ],
        'param_dyn_ukey' => 'product_price',
    ];
}
