<?php

namespace Model\CMS;

class Component_script {

    use \doctrine\Dashes\Model {
        \doctrine\Dashes\Model::create as protected _create;
        \doctrine\Dashes\Model::update as protected _update;
    }
    
    protected $table = 'component_script';
//    protected $recursive = \HBasis\HASMANY;
//    protected $relatedRecursive = \HBasis\HASMANY;
    public $foreignKeys = [
        'component' => [
            'type' => \HBasis\BELONGSTO,
            'key' => 'component_id',
            'model' => '\Model\CMS\Component'
        ],
    ];
    protected $fieldsFormat = [
        'alias' => ':',
    ];

    public function format_alias($field, $value, $format, $data) {
        if ($value === NULL) return false; // variable not used/changed on the proccess
        
        return preg_replace("/\s+/", '', $value);
    }

    public function create($data) {
        $id = $this->_create($data);
        $data['id'] = $id;
        $this->createFiles($data);

        return $id;
    }

    public function update($id, $data) {
        $res = $this->_update($id, $data);
        $this->createFiles($data);

        return $res;
    }
    
    public function createFiles($script) {
        $baseDir = APPPATH . '../' . $this->getScriptsDir();
        $precompiledDir = APPPATH . '../' . $this->getPreCompiledData($script['file_ext_precompiled'])[0];
        
        $filename = $this->getFileName($script);
        $precompiledFilename = $this->getPrecompiledFileName($script);
        
        $filepath = $baseDir . $filename;
        $precompiledFilepath = $precompiledDir . $precompiledFilename;
        
        if (!empty($script['file_ext']) && !file_exists($filepath)) {
            touch($filepath);
            chmod($filepath, 0664);
        }
        
        if (!empty($script['file_ext_precompiled']) && !file_exists($precompiledFilepath)) {
            touch($precompiledFilepath);
            chmod($precompiledFilepath, 0777);
        }
    }
    
    public function getComponentBaseDir() {
        return 'public/cpn/';
    }
    
    public function getScriptsDir() {
        return $this->getComponentBaseDir() . 'scripts/';
    }
    
    public function getPreCompiledData($str) {
        $str = explode('/', $str);
        if(count($str) > 1) {
            $str[0] = $this->getComponentBaseDir() . $str[0] . '/';
            return $str;
        } else {
            return [$this->getScriptsDir(), $str[0]];
        }
    }
    
    public function buildFileName($script) {
        return $_SESSION['config']['systemAlias'] . '_' . $script['alias'] . '-' . $script['id'];
    }
    
    public function getFileName($script) {
        return $this->buildFileName($script) . '.' . $script['file_ext'];
    }
    
    public function getPrecompiledFileName($script) {
        $precompiled = $this->getPreCompiledData($script['file_ext_precompiled']);
        strpos($precompiled[1], '.')!==0 && ($precompiled[1] = '.' . $precompiled[1]);
        return $this->buildFileName($script) . $precompiled[1];
    }

}
