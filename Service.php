<?php

namespace Model\CMS;

class Service {

    use \doctrine\Dashes\Model,
        \Model\CMS\Component_datatype_behavior,
        Generic_tag {
        \doctrine\Dashes\Model::find as protected _find;
        \doctrine\Dashes\Model::getBy as protected _getBy;
    }

    protected $modelAttrDefaults = [
        'table' => 'service',
//    protected $recursive = \HBasis\HASMANY;
        'foreignKeys' => [
            'param' => [
                'type' => \HBasis\HASMANY,
                'key' => 'service_id',
                'model' => '\Model\CMS\Service_param'
            ],
            'tag' => [
                'type' => \HBasis\HASMANY,
                'key' => 'service_id',
                'model' => '\Model\CMS\Service_tag'
            ],
        ],
        'fieldsFormat' => [
            'created' => ':',
            'created_by' => ':',
            'modified' => ':',
            'modified_by' => ':',
        ],
    ];

    public function getList($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $conditions['status'] = '1';
//        $conditions['analised'] = self::an_allow;

        return $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

    public function getBy($conditions = array(), $columns = null, $orderby = null, $recursive = null) {
        array_search('status IS NOT NULL', $conditions) === false && ($conditions['status'] = '1');
//        $conditions['analised'] = self::an_allow;

        $item = $this->_getBy($conditions, $columns, $orderby, $recursive);
        return $item;
    }

    public function find($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $list = ($this->_find($conditions, $limit, $page, $columns, $orderby, $recursive));
        return $list;
    }

    public $datatypeServiceIdList = [];

    public function datatype($item, $categoryId = NULL, $settings = [], $data = []) {
        $mySets = $this->_getSettings($settings, !empty($item['alias']) ? $item['alias'] : \Crush\Basic::getClassShortName($this));
        
        $criteria = [];


        !empty($mySets['hide_duplicates']) && !empty($this->datatypeServiceIdList) && ($criteria['id NOT'] = $this->datatypeServiceIdList);

        $data = array_merge($data, $mySets);
        
        if (!empty($mySets['filter'])) {
            !empty($data['filter_title']) && ($criteria['title LIKE'] = '%' . $data['filter_title'] . '%');
            if (!empty($data['filter_text'])) {
                $filtertext = $data['filter_text'];

                $filterCriteria = [];
                $filterCriteria[] = 'title LIKE "%' . $filtertext . '%"';

                $criteria[] = '(' . implode(' OR ', $filterCriteria) . ')';
            }
        }
        $results = [];
        $results = ($this->execCommonFind($this, $criteria, $categoryId, $mySets));
        $this->datatypeServiceIdList = array_merge($this->datatypeServiceIdList, \Crush\Collection::transform($results, '', ['id'], ['flatten']));

        if (!empty($_GET['debug']) && !empty($mySets['tagsId'])) {
            printf('<pre>%s</pre>', var_export($mySets, true));
            printf('<pre>%s</pre>', var_export($this->db, true));
            die;
        }
        
        if(!empty($mySets['pagination'])) {
            $results = $this->setPagination($results, $this, $criteria, $categoryId, $mySets);
        }
        
        return $results;
    }

}
