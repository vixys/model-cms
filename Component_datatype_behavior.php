<?php

namespace Model\CMS;

trait Component_datatype_behavior {

    protected static $settingsKey = '_feed';

    public function _getSettings($sets, $method, $required = false) {
        $method = preg_replace('/_model/','',strtolower($method));
        $mySets = [];
        if (!empty($sets[static::$settingsKey]) && isset($sets[static::$settingsKey][$method])) {
            $mySets = $sets[static::$settingsKey][$method];
        } else if ($required) {
            throw new \Exception('Settings missing for method : ' . $method);
        }

        return $mySets;
    }

    public function execCommonFind($model, $criteria = [], $categoryId = NULL, $mySets = [], $order = []) {
        $criteria = $this->buildCommonCriteria($criteria, $categoryId, $mySets);
        $order = $this->buildCommonOrder($criteria, $categoryId, $mySets, $order);

        (!array_key_exists('limit', $mySets) || (empty($mySets['limit']) && $mySets['limit']!=='0')) && ($mySets['limit'] = 10);
        $mySets['limit'] = (int) $mySets['limit'];
        (!array_key_exists('page', $mySets)) && ($mySets['page'] = 0);
        $mySets['page'] = (int) $mySets['page'];

        $results = [];
        
        method_exists($model, 'setRowActionExecute') && ($model->setRowActionRecursive(\HBasis\HASMANY)->setRowActionExecute());
        !empty($mySets['limit']) && ($results = $this->applyCommonFilters($model->getList($criteria, @$mySets['limit'], $mySets['page'], NULL, implode(', ', $order)), $mySets));
        
        /* custom parameters */
        !empty($this->getAttr('foreignKeys')['param']) && $results = array_map(function($p){
            !empty($p['param']) && ($p['param'] = \Crush\Collection::transform($p['param'], 'dynamic_ukey', ['value'], ['flatten']));
            return $p;
        }, $this->findHasMany($results, ['param']));
        /**/
        
        if(!empty($mySets['get_tags']) && method_exists($this, 'getTagList')) {
            foreach($results as $x => $result) {
                $results[$x] = $this->getTagList($result);
            }
        }
        if(!empty($mySets['pagination'])) {
            $results = $this->setPagination($results, $this, $criteria, $categoryId, $mySets);
        }
        
        if(empty($results)) {
            \Crush\Log::get(['id'=>'page-redirect'], NULL, false)
                    ->add((string) @$_SERVER['REQUEST_URI']);
        }
        
        return $results;
    }
    
    public function setPagination($results, $model, $criteria = [], $categoryId = NULL, $mySets = [], $order = []) {
        $results = ['rows' => $results];
        $results['pagination'] = [];
        $results['pagination']['total'] = (int) $this->execCommonCount($model, $criteria, $categoryId, $mySets, $order)[0]['rows'];
        $results['pagination']['pages'] = empty($mySets['limit']) ? 1 : ceil(($results['pagination']['total'] / $mySets['limit'])-1);
        $results['pagination']['page'] = (int) @$mySets['page'];
        $results['pagination']['page_list'] = $results['pagination']['pages'] > 0 ? (array) @range(0, $results['pagination']['pages']) : [];
//            empty($results['pagination']['page_list']) && ($results['pagination']['page_list'] = [0]);

        $results['pagination']['first_page'] = @$results['pagination']['page_list'][0];
        $results['pagination']['last_page'] = count($results['pagination']['page_list']) > 1 ? $results['pagination']['page_list'][count($results['pagination']['page_list'])-1] : 0;
        
        return $results;
    }

    public function execCommonCount($model, $criteria = [], $categoryId = NULL, $mySets = [], $order = []) {
        $criteria = $this->buildCommonCriteria($criteria, $categoryId, $mySets);
        $order = $this->buildCommonOrder($criteria, $categoryId, $mySets, $order);

        $results = ($model->getList($criteria, NULL, NULL, ['count(*) rows'], implode(', ', $order)));
        
        return $results;
    }

    public function buildCommonOrder($criteria = [], $categoryId = NULL, $mySets = [], $order = []) {

        if(!empty($mySets['orderby'])) {
            !is_array($mySets['orderby']) && ($mySets['orderby'] = [$mySets['orderby']]);
            foreach($mySets['orderby'] as $orderby) {
                $order[] = $orderby;
            }
        }
        !empty($mySets['order']) && ($order[] = 'order ASC');

        !empty($mySets['recent']) && (string) $mySets['recent'] === '1' && ($mySets['recent'] = 'created');
        !empty($mySets['recent']) && ($order[] = $mySets['recent'] . ' DESC');

        !empty($mySets['views']) && ($order[] = 'views DESC');


        !empty($mySets['az']) && (string) $mySets['az'] === '1' && ($mySets['az'] = 'name');
        !empty($mySets['az']) && ($order[] = $mySets['az'] . ' ASC');

        !empty($mySets['random']) && ($order[] = 'RAND()');

        return $order;
    }

    public function buildCommonCriteria($criteria = [], $categoryId = NULL, $mySets = []) {
        !is_array($criteria) && empty($criteria) && ($criteria = []);
        
        if (!empty($mySets['filter'])) {
            array_key_exists('filter_slug', $mySets) && 
                (!empty($mySets['filter_slug']) || !empty($mySets['filter_slug_empty'])) && 
                ($criteria['slug'] = empty($mySets['filter_slug']) ? ' ' : $mySets['filter_slug'] );
            !empty($mySets['filter_publisher']) && ($criteria['publisher_id'] = $mySets['filter_publisher']);
            !empty($mySets['filter_category']) && ($criteria['category_id'] = $mySets['filter_category']);
            if(!empty($mySets['filter_category_slug'])) {
                strpos($mySets['filter_category_slug'], '%')!==false && ($mySets['filter_category_slug'] = urldecode($mySets['filter_category_slug']));
                $category = $this->loadFkModel('category')->find(['slug'=>explode('|',$mySets['filter_category_slug'])]);
                $mySets['filter_category'] = !empty($category) ? \Crush\Collection::transform($category, '', ['id'], ['flatten']) : '0';
                ($criteria['category_id'] = $mySets['filter_category']);
            }
        }
        
        if(!empty($mySets['criteria'])) {
            foreach($mySets['criteria'] as $x => $y) {
                if(empty($y)) {
                    /* avoid id fields with empty value to break the where clausule */
                    unset($mySets['criteria'][$x]);
                } else if(preg_match('/id$/', $x) && is_string($y) && preg_match('/\D+/', $y, $splitter)) {
                    /* split ids */
                    $z = explode($splitter[0], $y);
                    count($z) > 1 && $z[0]==='' && array_shift($z);
                    
                    $splitter[0] = trim($splitter[0]);
                    $operator = (!empty($splitter[0]) && !in_array($splitter[0], ['<>', 'LIKE', '=', 'NOT LIKE', 'NOT IN', 'IN']) ? '' : ' '.$splitter[0]);
                    if(!empty($operator) && !empty($mySets['criteria'][$x])) { // to fix slug integration conflict
                        unset($mySets['criteria'][$x]);
                    }
                    
                    $mySets['criteria'][$x . $operator] = $z;
                }
            }
        }
        
        $criteria = array_merge((array) @$mySets['criteria'], $criteria);
        
        !empty($categoryId) && empty($criteria['category_id']) && ($criteria['category_id'] = $categoryId);
        
        !empty($mySets['category']) && ($criteria['category_id'] = $mySets['category']);
        
        !empty($mySets['publisher_id']) && ($criteria['publisher_id'] = $mySets['publisher_id']);
        
        /* sub categories */
        if(!empty($mySets['subcategories']) && !empty($criteria['category_id'])) {
            $criteria[] = 'category_id IN (SELECT id FROM category WHERE category_id = '.$criteria['category_id'].' OR id = '.$criteria['category_id'].')';
            unset($criteria['category_id']);
        }
        
        /* tags */
        if(!empty($mySets['tags']) && empty($mySets['tagsId'])) {
            $tagBridgeModel = $this->loadModelInstance($this->getAttr('foreignKeys')['tag']['model']);
            $this->loadModelInstance($tagBridgeModel->getAttr('foreignKeys')['tag']['model']);
            $mySets['tags'] = $this->model['Tag']->identifyTags($mySets['tags']);
            $mySets['tagsId'] = implode(',',\Crush\Collection::transform((array)$this->model['Tag']->find(['description'=>$mySets['tags']]), '', ['id'], ['flatten']));
        }
        
        if(!empty($mySets['tagsId'])) {
            $tagBridgeModel = $this->loadModelInstance($this->getAttr('foreignKeys')['tag']['model']);
            $criteria[] = '(SELECT COUNT(*) FROM '.$tagBridgeModel->getAttr('table').' WHERE '.$tagBridgeModel->getAttr('table').'.tag_id IN('.$mySets['tagsId'].') AND '.$tagBridgeModel->getAttr('table').'.'.$this->getAttr('foreignKeys')['tag']['key'].' = '.$this->getAttr('table').'.'.$this->getAttr('primaryKey').') > 0';
        }
        
        /* filter params */
//        $mySets['filter_param'] = [];
//        $mySets['filter_param'][] = ['ukey'=>'restrict','value'=>'1','status'=>1];
        if(!empty($mySets['filter_param'])) {
            $paramBridgeModel = $this->loadModelInstance($this->getAttr('foreignKeys')['param']['model']);
            $dynModel = $this->loadModel('\Model\Dynamic');
            foreach($mySets['filter_param'] as $filterParam) {
                if((array_key_exists('status', $filterParam) && empty($filterParam['status'])) || $filterParam['value']==='') continue;
                
                $criteria[] = '(SELECT COUNT(*) FROM '.$paramBridgeModel->getAttr('table').' WHERE '.
                    $paramBridgeModel->getAttr('table').'.value = \''.$filterParam['value'].'\'  AND '.
                    $paramBridgeModel->getAttr('table').'.'.$this->getAttr('foreignKeys')['param']['key'].' = '.$this->getAttr('table').'.'.$this->getAttr('primaryKey').' AND '.
                    $paramBridgeModel->getAttr('table').'.dynamic_ukey = \''.$filterParam['ukey'].'\' '
                    .') > 0';
                
            }
        }
        
        $criteria['status'] = '1';
        
        // get a record by its id
        if (!empty($mySets['select'])) {
            $criteria['id'] = $mySets['select'];
        }
        
        // do not get the records that are present on the given id list
        if (!empty($mySets['except'])) {
            $criteria['id NOT'] = is_array($mySets['except']) ? $mySets['except'] : [$mySets['except']];
        }

        return $criteria;
    }
    
    public function applyCommonFilters($results, $mySets) {
        // it applies the key sent as key of the array and group the items that have the same value for that key
        !empty($mySets['coll_key']) && ($results = \Crush\Collection::transform($results, $mySets['coll_key'], [], ['array']));
        // it applies the key sent as key (be carefull with items having the same key)
        !empty($mySets['arr_key']) && ($results = \Crush\Collection::transform($results, $mySets['arr_key']));
        
        return $results;
    }

}
