<?php

namespace Model\CMS;

class Announce_tag extends Generic_bridge_tag {

    protected $modelAttrDefaults = [
        'table' => 'announce_tag',
        'foreignKeys' => [
            'announce' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'announce_id',
                'model' => '\Model\CMS\Announce'
            ],
            'tag' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'tag_id',
                'model' => '\Model\CMS\Tag'
            ],
        ],
    ];

}
