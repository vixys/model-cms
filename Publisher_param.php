<?php

namespace Model\CMS;

class Publisher_param extends \Model\CMS\Generic_param {

    protected $modelAttrDefaults = [
        'table' => 'publisher_param',
        'foreignKeys' => [
        ],
        'param_dyn_ukey' => 'publisher',
    ];
}
