<?php

namespace Model\CMS;

class Webservice_datatype {

    use \Model\CMS\Component_datatype_behavior;

    public function urlGet($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, !empty($item['alias']) ? $item['alias'] : \Crush\Basic::getClassShortName($this));
        $url = $this->addGetData($mySets['url'], (array) @$mySets['get']);
        
        /* temp implementation. substitute with a filter for value */
        !empty($mySets['environment']) && ($url = \acsp\helpers\Url::ambienteUrl($url));
        /**/
        
        $response = file_get_contents($url);
        
        $formatted = $this->format($response, @$mySets['format']);
        $ordered = $this->order($formatted, (string) @$mySets['order']);
        
        $result = $this->output($ordered, @$mySets['output']);
        return $result;
    }
    
    public function addGetData($url, $get) {
        $urlget = [];
        
        if(!empty($get))
        foreach($get as $x => $y)
            $urlget[] = $x.'='.$y;
        
        $urlget = implode('&', $urlget);
        $url .= (strpos($url, '?')===false ? '?' : '&') . $urlget;
        return $url;
    }
    
    public function format($data, $format) {
        switch($format) {
            case 'json':
                $result = json_decode($data, true);
                break;
            
            default:
                $result = $data;
                break;
        }
        
        return $result;
    }
    
    public function order($data, $order) {
        $order = explode(':', $order);
        $result = [];
        switch($order[0]) {
            case 'field':
                $keys = explode(',', $order[1]);
                foreach($data as $x => $row) {
                    $keyStr = '';
                    foreach($keys as $key) {
                        $keyStr .= $row[$key];
                    }
                    $keyStr .= rand(100, 999) . rand(100, 999);
                    
                    $result[$keyStr] = $row;
                }
                ksort($result);
                break;
            
            default:
                $result = $data;
                break;
        }
        
        return $result;
    }
    
    public function output($data, $format) {
        switch($format) {
            case 'json':
                $result = json_encode($data);
                break;
            
            default:
                $result = $data;
                break;
        }
        
        return $result;
    }

}
