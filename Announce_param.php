<?php

namespace Model\CMS;

class Announce_param extends \Model\CMS\Generic_param {

    protected $modelAttrDefaults = [
        'table' => 'announce_param',
        'foreignKeys' => [
        ],
        'param_dyn_ukey' => 'announce',
    ];
}
