<?php

namespace Model\CMS;

class Place {

    use \doctrine\Dashes\Model,
        \Model\CMS\Component_datatype_behavior,
        Generic_tag {
        \doctrine\Dashes\Model::getBy as protected _getBy;
    }

    protected $modelAttrDefaults = [
        'table' => 'place',
        'foreignKeys' => [
            'category' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'category_id',
                'model' => '\Model\CMS\Category'
            ],
        ],
        'fieldsFormat' => [
//            'publish_date' => ['/(\d{2})\/(\d{2})\/(\d{4})\s(\d{2})\:(\d{2})/', '$3-$2-$1 $4:$5'],
            'category_id' => ':',
            'created' => ':',
            'modified' => ':',
        ],
    ];

    public function format_category_id($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        return !empty($data['category_id']) ? $data['category_id'] : null;
    }

    public function getList($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $conditions['status'] = '1';

        return $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

    public function getBy($conditions = array(), $columns = null, $orderby = null, $recursive = null) {
        $conditions['status'] = '1';

        $item = $this->_getBy($conditions, $columns, $orderby, $recursive);
        return $item;
    }

    public function datatype($item, $categoryId = NULL, $settings = [], $data = []) {
        $mySets = $this->_getSettings($settings, !empty($item['alias']) ? $item['alias'] : \Crush\Basic::getClassShortName($this));

        $criteria = [];
        $data = array_merge($data, $mySets);
        
        /* abstract soon. define common columns for table of contents */
        if (!empty($mySets['filter'])) {
            !empty($data['filter_title']) && ($criteria['title LIKE'] = '%' . $data['filter_title'] . '%');
            !empty($data['filter_category']) && ($criteria['category_id'] = $data['filter_category']);
            
        }
        

        $results = $this->execCommonFind($this, $criteria, $categoryId, $mySets);
        return $results;
    }

}
