<?php

namespace Model\CMS;

class Menu {

    use \doctrine\Dashes\ControlRecord,
        \Model\CMS\Component_datatype_behavior {
        \doctrine\Dashes\ControlRecord::find as protected _find;
        \doctrine\Dashes\ControlRecord::getBy as protected _getBy;
    }

    protected $modelAttrDefaults = [
        'table' => 'menu',
        'recursive' => \HBasis\BELONGSTO,
        //'relatedRecursive' => \HBasis\HASMANY,
        'foreignKeys' => [
            'parent' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'menu_id',
                'model' => '\Model\CMS\Menu'
            ],
            'child' => [
                'type' => \HBasis\HASMANY,
                'key' => 'menu_id',
                'model' => '\Model\CMS\Menu',
                'order' => 'order ASC'
            ],
            'page' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'page_id',
                'model' => '\Model\CMS\Page'
            ],
            'post' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'post_id',
                'model' => '\Model\CMS\Post'
            ],
            'category' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'category_id',
                'model' => '\Model\CMS\Category'
            ],
            'param' => [
                'type' => \HBasis\HASMANY,
                'key' => 'menu_id',
                'model' => '\Model\CMS\Menu_param'
            ],
        ],
        'fieldsFormat' => [
            'created' => ':',
            'modified' => ':',
            'slug' => ':',
            'menu_id' => ':',
            'category_id' => ':',
            'post_id' => ':',
            'page_id' => ':',
            'level' => ':',
        ],
    ];

    public function format_level($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        $level = empty($data['menu_id']) ? "1" : (int) $this->get($data['menu_id'])['level'] + 1;
        return $level;
    }

    public function format_menu_id($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        return !empty($data['menu_id']) ? $data['menu_id'] : null;
    }

    public function format_category_id($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        return !empty($data['category_id']) ? $data['category_id'] : null;
    }

    public function format_post_id($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        return !empty($data['post_id']) ? $data['post_id'] : null;
    }

    public function format_page_id($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        return !empty($data['page_id']) ? $data['page_id'] : null;
    }

    public function format_order($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        if (empty($data[$this->getAttr('primaryKey')])) {
            $order = (int) $this->max('menu', 'order', ['menu_id' => $post['menu_id']]);
            return $order + 1;
        }
        return $value;
    }

    public function findTree($conditions = []) {
        $list = $this->find($conditions, NULL, NULL, NULL, 'order ASC', -1);
        return (array) @\Crush\Collection::array_tree_to_single(\Crush\Collection::convertToTree($list, 'id', 'menu_id'));
    }

    public function getIndexTree($conditions = []) {
        $list = $this->getTreeList($conditions, null, null, null, 'order ASC', false);
        $list = $this->flatTree($list);

        return $list;
    }

    public function formatTree($list, $level = 0) {
        foreach ($list as $x => $item) {
            $item['title2'] = $item['title'];
            if ($level > 0) {
                $item['title2'] = str_pad('', $level, '-', STR_PAD_LEFT) . ' ' . $item['title'];
            }
            if (!empty($item['child'])) {
                $item['child'] = $this->formatTree($item['child'], $level + 1);
            }

            $list[$x] = $item;
        }

        return $list;
    }

    public function flatTree($list) {
        $results = [];
        foreach ($list as $x => $item) {
            $childList = (array) @$item['child'];
            unset($item['child']);
            $results[] = $item;

            if (!empty($childList)) {
                $results = \Crush\Collection::unify($results, $this->flatTree($childList));
            }
        }

        return $results;
    }

    /**
     * Get a recursive list of menus from the root ones
     * @param array $conditions
     * @param integer $limit
     * @param integer $page
     * @param array $columns
     * @param string $orderby
     * @param integer $recursive
     * @return array
     */
    public function getTreeList($conditions = [], $limit = null, $page = null, $columns = null, $orderby = null, $recursive = true) {
        $this->setFkExecutionMode(['post', 'page', 'category']);
        
        foreach(['post', 'page', 'category', 'child'] as $fk) $this->loadModelInstance($this->foreignKeys[$fk]['model']);
//        $conditions[] = 'menu_id IS NULL';
//        $this->recursive = \HBasis\HASMANY;
//        
//        unset($this->foreignKeys['post']);
//        unset($this->foreignKeys['page']);

        /* avoid to get a big list of hasmany related to the models included on this process */
        $this->model['Menu']->explicitAttr('foreignKeys');
        $this->model['Page']->explicitAttr('foreignKeys');
        $this->model['Post']->explicitAttr('foreignKeys');
        $this->model['Category']->explicitAttr('foreignKeys');
        
        
        $pageForeignKeys = $this->model['Page']->foreignKeys;
        $postForeignKeys = $this->model['Post']->foreignKeys;
        $categoryForeignKeys = $this->model['Category']->foreignKeys;
        $this->model['Page']->foreignKeys = [];
        $this->model['Post']->foreignKeys = [];
        $this->model['Category']->foreignKeys = [];

        $foreignKeys = $this->foreignKeys;
        unset($this->foreignKeys['parent']);
        unset($this->foreignKeys['child']);
        if (!$recursive) {
            unset($this->foreignKeys['post']);
            unset($this->foreignKeys['page']);
            unset($this->foreignKeys['category']);
        }

        /* apply order to recursive childs */
//        $defaultOrderBy = $this->foreignKeys['child']['order'];
//        $this->foreignKeys['child']['order'] = $orderby;
//        $this->model['Menu']->foreignKeys['child']['order'] = $orderby;

        $list = $this->getList($conditions, $limit, $page, $columns, $orderby);
        $list = \Crush\Collection::transform($list, 'id');
        
        if(!empty($list)) {
        
            $childConditions = array_merge($conditions, ['menu_id'=>array_keys($list)]);
            $childTreeList = $this->model['Menu']->getTreeList($childConditions, $limit, $page, $columns, $orderby, $recursive);
            $childList = \Crush\Collection::transform($childTreeList, 'menu_id', [], ['array']);
            
            if(!empty($childList))
            foreach($list as $menuId => $menu) {
                $list[$menuId]['child'] = [];
                !empty($childList[$menuId]) && ($list[$menuId]['child'] = (is_array($childList[$menuId]) ? $childList[$menuId] : []) );
            }
        }
        

//        $this->relatedConditions = ['status' => '1'];
//        $this->relatedRecursive = \HBasis\HASMANY;
//        $list = $this->findHasMany($list, ['child']);
//        $this->relatedConditions = [];


        /* restore order to recursive childs */
//        $this->foreignKeys['child']['order'] = $defaultOrderBy;

        $this->relatedRecursive = NULL;
        $this->foreignKeys = $foreignKeys;
        $this->model['Page']->foreignKeys = $pageForeignKeys;
        $this->model['Post']->foreignKeys = $postForeignKeys;
        $this->model['Category']->foreignKeys = $categoryForeignKeys;

        !empty($this->getAttr('foreignKeys')['param']) && $list = array_map(function($p){
            !empty($p['param']) && ($p['param'] = \Crush\Collection::transform($p['param'], 'dynamic_ukey', ['value'], ['flatten']));
            return $p;
        }, $this->findHasMany($list, ['param']));

        return $list;
    }

    public function getList($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        !array_key_exists('status', $conditions) && ($conditions['status'] = '1');
        if(array_key_exists('status', $conditions) && empty($conditions['status']) && (string) $conditions['status']!=='0') unset($conditions['status']);

        $results = $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
        return $results;
    }

    public function getBy($conditions = array(), $columns = null, $orderby = null, $recursive = null) {
        !array_key_exists('status', $conditions) && ($conditions['status'] = '1');
        if(array_key_exists('status', $conditions) && empty($conditions['status']) && (string) $conditions['status']!=='0') unset($conditions['status']);

        $item = $this->_getBy($conditions, $columns, $orderby, $recursive);
        return $item;
    }

    protected $relatedConditions = [];

    public function find($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $conditions = array_merge($this->relatedConditions, $conditions);

        $list = $this->_find($conditions, $limit, $page, $columns, $orderby, $recursive);
        $list = $this->formatList($list);
        return $list;
    }

    public function formatList($list) {
        if(!empty($list))
        foreach ($list as $x => $item) {
            $item['title2'] = $item['title'];
            $item['level'] = (int) $item['level'];
            if ($item['level'] > 1) {
                $item['title2'] = str_pad('', (int) $item['level'] - 1, '-', STR_PAD_LEFT) . ' ' . $item['title'];
            }

            $list[$x] = $item;
        }
        return $list;
    }

    public function datatype($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, !empty($item['alias']) ? $item['alias'] : \Crush\Basic::getClassShortName($this));

        $criteria = [];

        empty($criteria['menu_id']) && ($criteria['menu_id'] = NULL);
        !empty($mySets['menu_id']) && ($criteria['menu_id'] = $mySets['menu_id']);

        $order = [];
        !empty($mySets['order']) && ($order[] = 'order ASC');

        $order = implode(', ', $order);

        $this->setFkExecutionMode(['post', 'page', 'category', 'child']);
        method_exists($this, 'setRowActionExecute') && ($this->setRowActionRecursive(\HBasis\HASMANY)->setRowActionExecute());
        
        $results = !empty($mySets['tree']) ? $this->getTreeList($criteria, null, null, null, $order) : $this->execCommonFind($this, [], $categoryId, $mySets);
        $results = \Crush\Collection::transform($results, '');
        return $results;
    }

    public function setFkExecutionMode($fkList) {
        $this->explicitAttr('foreignKeys');
        
        foreach($fkList as $fk) {
        /* to get private records */
            $model = $this->loadModelInstance($this->foreignKeys[$fk]['model']);
            method_exists($model, 'setRowActionExecute') && ($model->setRowActionRecursive(\HBasis\HASMANY)->setRowActionExecute());
        }
        return $this;
    }

}
