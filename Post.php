<?php

namespace Model\CMS;

class Post {

    use \doctrine\Dashes\ControlRecord,
        \Model\CMS\Component_datatype_behavior,
        Generic_tag {
            \doctrine\Dashes\ControlRecord::find as protected _find;
            \doctrine\Dashes\ControlRecord::getBy as protected _getBy;
    }

    protected $modelAttrDefaults = [
        'table' => 'post',
//    protected $recursive = \HBasis\HASMANY;
        'foreignKeys' => [
            'category' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'category_id',
                'model' => '\Model\CMS\Category'
            ],
            'publisher' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'publisher_id',
                'model' => '\Model\CMS\Publisher'
            ],
            'tag' => [
                'type' => \HBasis\HASMANY,
                'key' => 'post_id',
                'model' => '\Model\CMS\Post_tag'
            ],
            'post_gallery' => [
                'type' => \HBasis\HASMANY,
                'key' => 'post_id',
                'model' => '\Model\CMS\Post_gallery'
            ],
            'param' => [
                'type' => \HBasis\HASMANY,
                'key' => 'post_id',
                'model' => '\Model\CMS\Post_param'
            ]
        ],
        'fieldsFormat' => [
            'publish_date' => ['/(\d{2})\/(\d{2})\/(\d{4})\s(\d{2})\:(\d{2})/', '$3-$2-$1 $4:$5'],
            'category_id' => ':',
            'pic_small' => ':',
            'pic_large' => ':',
            'created' => ':',
            'created_by' => ':',
            'modified' => ':',
            'modified_by' => ':',
            'slug' => ':',
            'old_slug' => ':',
        ],
    ];

    const an_none = 0;
    const an_allow = 1;
    const an_deny = 2;

    public $enum = [
        'analysis' => [
            'none' => self::an_none,
            'allow' => self::an_allow,
            'deny' => self::an_deny,
        ]
    ];

    public function format_category_id($field, $value, $format, $data) {
        if ($value === NULL && !isset($data['category_id']))
            return false;

        if (empty($value)) {
            return NULL;
        }

        return $value;
    }

    public function format_pic_small($field, $value, $format, $data) {
        if ($value === NULL && !isset($data['pic_large']))
            return false;

        $value = (string) @$data['pic_large'];
        if (empty($value)) {
            return NULL;
        }
        return preg_replace("/^\//", '', $value);
    }

    public function format_pic_large($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        if (empty($value)) {
            return NULL;
        }
        return preg_replace("/^\//", '', $value);
    }

    /**
     * Keeps the number of page views
     */
    public function addView($item) {
        is_array($item) ? ($id = $item['id']) : ($id = $item);

        if (empty(@$_SERVER['REQUEST_TRUST']) && !$this->getData($id)) {
            is_int($item) && ($item = $this->get($id));

            $updateData = [];
            $updateData['id'] = $item['id'];
            $updateData['views'] = (int) $item['views'] + 1;
            $this->update($id, $updateData);
            $this->addData($id, true);
            return true;
        }

        return false;
    }

    public function resolve($id, $action) {
        if (in_array($action, array_keys($this->enum['analysis']))) {
            $item = $this->get($id);

            $item['analised'] = $this->enum['analysis'][$action];
            $item['analised_date'] = date('Y-m-d H:i:s');
            $item['analised_by'] = (int) @\acsp\helpers\Auth::getUserData()['id'];

            $this->save($item);
        }

        return false;
    }

    public function getList($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $conditions['publish_date <='] = date('Y-m-d H:i');
        $conditions['status'] = '1';
//        $conditions['analised'] = self::an_allow;

        return $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

    public function getBy($conditions = array(), $columns = null, $orderby = null, $recursive = null) {
        array_search('publish_date IS NOT NULL', $conditions) === false && $conditions['publish_date <='] = date('Y-m-d H:i');
        array_search('status IS NOT NULL', $conditions) === false && ($conditions['status'] = '1');
//        $conditions['analised'] = self::an_allow;

        $item = $this->_getBy($conditions, $columns, $orderby, $recursive);
        return $item;
    }

    public function find($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $list = $this->prepare_list($this->_find($conditions, $limit, $page, $columns, $orderby, $recursive));
        return $list;
    }

    public function prepare_list($results) {
        if (!empty($results)) {
            foreach ($results as $x => $result) {
                $result['_indirect_route'] = '';
                $result['_direct_route'] = '';

                !empty($result['slug']) && ($result['_direct_route'] = 'publicacao/' . @$result['slug']);
                !empty($result['old_slug']) && ($result['_direct_old_route'] = 'publicacao/' . @$result['old_slug']);
                if (!empty($result['category']['slug'])) {
                    !empty($result['slug']) && ($result['_route'] = 'categoria/' . $result['category']['slug'] . '/' . $result['slug']);
                    !empty($result['old_slug']) && ($result['_old_route'] = 'categoria/' . $result['category']['slug'] . '/' . $result['old_slug']);
                    $result['_cat_route'] = 'categoria/' . $result['category']['slug'];
                    $result['_indirect_route'] = @$result['_route'];
                    $result['_indirect_old_route'] = @$result['_old_route'];
                } else {
                    $result['_old_route'] = (string) @$result['_direct_old_route'];
                    $result['_route'] = $result['_direct_route'];
                    $result['_cat_route'] = '';
                }

                $results[$x] = $result;
            }
        }

        return $results;
    }

    public $datatypePostIdList = [];

    public function datatype($item, $categoryId = NULL, $settings = [], $data = []) {
        $mySets = $this->_getSettings($settings, !empty($item['alias']) ? $item['alias'] : \Crush\Basic::getClassShortName($this));
        
        $criteria = [];


        !empty($mySets['hide_duplicates']) && !empty($this->datatypePostIdList) && ($criteria['id NOT'] = $this->datatypePostIdList);
        in_array((string) @$mySets['ext_ref'], ['0', '1'], true) && ($criteria['type'] = $mySets['ext_ref']);

        !empty($mySets['pic_required']) && ($criteria[] = '(pic_large IS NOT NULL AND pic_small IS NOT NULL)');

        $data = array_merge($data, $mySets);
        
        /* abstract soon. define common columns for table of contents */
        !empty($mySets['recent']) && ($mySets['recent'] = 'publish_date DESC, priority');
        if (!empty($mySets['filter'])) {
            !empty($data['filter_title']) && ($criteria['title LIKE'] = '%' . $data['filter_title'] . '%');
            if (!empty($data['filter_text'])) {
                $filtertext = $data['filter_text'];

                $filterCriteria = [];
                $filterCriteria[] = 'title LIKE "%' . $filtertext . '%"';
                $filterCriteria[] = 'preview LIKE "%' . $filtertext . '%"';
                $filterCriteria[] = 'article LIKE "%' . $filtertext . '%"';
                $filterCriteria[] = '(SELECT id FROM publisher WHERE id = post.publisher_id AND name LIKE "%' . $filtertext . '%" ) IS NOT NULL';
                $filterCriteria[] = '(SELECT id FROM category WHERE id = post.category_id AND title LIKE "%' . $filtertext . '%" ) IS NOT NULL';

                $criteria[] = '(' . implode(' OR ', $filterCriteria) . ')';
            }
        }
        !empty($mySets['ext_ref']) && ($criteria[] = '(ref_link IS NOT NULL AND ref_link <> \'\')');
        $results = [];
        $fixResults = [];

        if (!empty($mySets['fixed'])) {
            $fixSettings = $mySets;
            $fixSettings['limit'] = $mySets['fixed'];
            $fixCriteria = $criteria;
            $fixCriteria['fix'] = 1;
            $criteria['fix'] = 0;
            $fixResults = ($this->execCommonFind($this, $fixCriteria, $categoryId, $fixSettings, ['fix DESC']));
            $this->datatypePostIdList = array_merge($this->datatypePostIdList, \Crush\Collection::transform($fixResults, '', ['id'], ['flatten']));
//            printf('<pre>%s</pre>', var_export($fixResults, true));die;
        }
        $results = ($this->execCommonFind($this, $criteria, $categoryId, $mySets));
        $this->datatypePostIdList = array_merge($this->datatypePostIdList, \Crush\Collection::transform($results, '', ['id'], ['flatten']));

        if (!empty($_GET['debug']) && !empty($mySets['tagsId'])) {
            printf('<pre>%s</pre>', var_export($mySets, true));
            printf('<pre>%s</pre>', var_export($this->db, true));
            die;
        }
        
        $results = array_merge($fixResults, $results);
        
        if(!empty($mySets['photos'])) {
            $results = $this->findHasMany($results, ['post_gallery']);
        }
        
        return $results;
    }

}
