<?php

namespace Model\CMS;

class Menu_param extends \Model\CMS\Generic_param {

    protected $modelAttrDefaults = [
        'table' => 'menu_param',
        'foreignKeys' => [
        ],
        'param_dyn_ukey' => 'menu',
    ];
}
