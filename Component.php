<?php

namespace Model\CMS;

class Component {

    use \doctrine\Dashes\Model;

    protected $modelAttrDefaults = [
        'table' => 'component',
        'foreignKeys' => [
//        'category' => [
//            'type' => \HBasis\BELONGSTO,
//            'key' => 'category_id',
//            'model' => '\Model\Category'
//        ],
//        'datatype' => [
//            'type' => \HBasis\BELONGSTO,
//            'key' => 'component_datatype_id',
//            'model' => '\Model\Component_datatype'
//        ],
            'component_datatype_component' => [
                'type' => \HBasis\HASMANY,
                'key' => 'component_id',
                'model' => '\Model\CMS\Component_datatype_component',
                'recursive' => \HBasis\HASMANY
            ],
            'component_settings' => [
                'type' => \HBasis\HASMANY,
                'key' => 'component_id',
                'model' => '\Model\CMS\Component_settings'
            ],
            'component_script' => [
                'type' => \HBasis\HASMANY,
                'key' => 'component_id',
                'model' => '\Model\CMS\Component_script'
            ],
            'page_component' => [
                'type' => \HBasis\HASMANY,
                'key' => 'component_id',
                'model' => '\Model\CMS\Page_component'
            ],
        ],
        'fieldsFormat' => [
            'size' => ':',
        ],
    ];

    public function format_size($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        return trim(preg_replace("/([^\d,])+/", '', $value));
    }

    public function getComponentScript($item) {
        !is_array($item) && ($item = $this->get($item));
        $item = $this->sortComponentScript($this->getHasMany($item, ['component_script']));

        return $item;
    }

    public function sortComponentScript($item) {
        if (!empty($item['component_script'])) {
            $item['component_script'] = \Crush\Collection::transform($item['component_script'], 'order+id');
            ksort($item['component_script']);
        }

        return $item;
    }

    public function getComponentSettings($item) {
        $item = $this->sortComponentSettings($this->getHasMany($item, ['component_settings']));

        return $item;
    }
    
    public function findComponentSettings($item, $conditions = []) {
        $this->loadModel($this->getAttr('foreignKeys')['component_settings']['model']);
        $conditions = array_merge(['component_id'=>$item['id']],$conditions);
        $settings = $this->sortComponentSettings($this->model['Component_settings']->find($conditions, null, null, null, null, -1));
        
        return $settings;
    }

    public function sortComponentSettings($item) {
        if (!empty($item['component_settings'])) {
            /* strpad to avoid conflict on ordering keys */
            foreach($item['component_settings'] as $x => $y) {
                $y['group11'] = str_pad($y['group'], 11, '0', STR_PAD_LEFT);
                $y['order11'] = str_pad($y['order'], 11, '0', STR_PAD_LEFT);
                $y['id11'] = str_pad($y['id'], 11, '0', STR_PAD_LEFT);

                $item['component_settings'][$x] = $y;
            }
            /**/
            
            $item['component_settings'] = \Crush\Collection::transform($item['component_settings'], 'group11+.+order11+.+id11', [], [
//                'keyPad' => [11, '0', STR_PAD_LEFT]
            ]);
            ksort($item['component_settings']);
        }

        return $item;
    }

    public function getFeeders($item) {
        $this->loadModelInstance($this->getAttr('foreignKeys')['component_datatype_component']['model']);
        $fks = $this->model['Component_datatype_component']->foreignKeys;
        unset($this->model['Component_datatype_component']->foreignKeys['component']);

        $item = $this->getHasMany($item, ['component_datatype_component']);

        $this->model['Component_datatype_component']->foreignKeys = $fks;
        return $item;
    }

    public function getDataFeed($component, $variables) {
        $feederList = (array) @$component['component_datatype_component'];
        $this->loadModelInstance($this->getAttr('foreignKeys')['component_datatype_component']['model']);
        $this->loadModelInstance($this->model['Component_datatype_component']->getAttr('foreignKeys')['datatype']['model']);
        extract($variables);

        $dataFeed = ['sets' => [], 'results' => [], 'linkresults' => []];
        if (!empty($feederList)) {
            foreach ($feederList as $item) {
                if (!empty($item['datatype'])) {
//                    $datatypeFunction = $item['datatype']['function'];

//                    if (!method_exists($this->model['Component_datatype'], $datatypeMethod)) {
//                        throw new \Exception('There is no method called : ' . $datatypeMethod);
//                    }

//                    list($datatypeObj, $datatypeMethod) = $this->getFeedMethod($datatypeFunction);

                    $dataFeed['sets'][$item['alias']] = $this->model['Component_datatype']->_getSettings($vars['sets'], $item['alias']);
                    $dataFeed['results'][$item['alias']] = (int)$item['json_output'] < 2 ? 
                            $this->getDataFeedResults($item, $categoryId, $dataFeed['sets'][$item['alias']], $data)
                                : [
                                    'link'=>'/page/componentFeed/'.implode('/', [$component['id'], $item['alias']]) . '?' . http_build_query($dataFeed['sets'][$item['alias']]), 
                                    'source'=>'/page/componentFeed/'.implode('/', [$component['id'], $item['alias']]) , 
                                    'sets' => $dataFeed['sets'][$item['alias']] ];
//                    if((int)$item['json_output'] === 2) {
////                        printf("<pre>%s</pre>\n", var_export($dataFeed['sets'][$item['alias']], true));
//                        printf("<pre>%s</pre>\n", var_export($dataFeed['results'][$item['alias']], true));die;
//                    }
                }
            }
        }
        return $dataFeed;
    }

    public function getDataFeedResults($item, $categoryId, $sets, $data=[]) {
        $datatypeFunction = $item['datatype']['function'];
        list($datatypeObj, $datatypeMethod) = $this->getFeedMethod($datatypeFunction);
        
        $vars = ['_feed'=>[$item['alias']=>$sets]];
        method_exists($datatypeObj, 'setRowActionExecute') && ($datatypeObj->setRowActionExecute());
        return $datatypeObj->{$datatypeMethod}($item, $categoryId, $vars, $data);
    }

    public function getFeedMethod($datatypeFunction) {
        $function = explode('/', $datatypeFunction);

        $model = $this->loadModelInstance($function[0]);
        $method = empty($function[1]) ? ('datatype') : $function[1];

        return [$model, $method];
    }

    public function readEntryList($text) {
        $function = explode('.', $text);
        return array_pad($function, 2, NULL);
    }

    public function compose($item, $categoryId, $settings, $data) {
        $feed = [];
        // add constants to page component with their default values realtime
        $componentSettingsModel = $this->loadModel($this->getAttr('foreignKeys')['component_settings']['model']);
        $constant = \Crush\Collection::transform((array)$this->findComponentSettings($item, ['entry_type'=> (int) $componentSettingsModel->getEntryItemInfoByUkey('const', 'order')]), 'entry_name',['default_value'],['flatten']);
        $settings = array_merge($settings, $constant);
        
        $builtSettings = $this->buildSettings($settings);
        $vars = $data;
        $vars['vars'] = $vars['page']['vars'];
        $vars['sets'] = $this->renderSettings($builtSettings, $vars);

        $item = $this->getFeeders($item);
        $dataFeed = $this->getDataFeed($item, ['categoryId' => $categoryId, 'vars' => $vars, 'data' => $data]);

        $uniqid = $vars['sets']['uniqid'];
        list($template, $jsonVarList) = $this->composeJsonScripts($item['template'], $item, $dataFeed['results'], $uniqid);

        $vars['_results'] = $dataFeed['results'];
        $vars['_scripts'] = $this->composeScriptsCode($item, $jsonVarList, $uniqid);
        if(!empty($vars['_scripts'])) {
            $template = $this->composeJsonSettings($template, $builtSettings, $uniqid, $vars);
        }

        /* compatibility var. it will be removed soon */
        $vars['results'] = @$vars['_results'][key($vars['_results'])];
        
//        $item['template'] .= @$builtSettings['_cat_color'];
        $result = $this->linkAs($item, static::CMS_RenderHTML($template, $vars), $vars);
        return ['html' => $result, 'sets' => $dataFeed['sets'], '_scripts' => $vars['_scripts']];
    }
    
    public function linkAs($item, $result, $vars) {
        switch((string) @$item['link_as']) {
            case '1':
            case '2':
                $ext = '.' . ((string) @$item['link_as'] === '1' ? 'css' : 'js');
                $filePath = '/tmp/component_compose_'.$vars['sets']['uniqid'] . $ext;
                if(@file_put_contents(__DIR__ . '/../../..' . $filePath, $result) === false) {
                    if(!empty($_GET['debug_tmp'])) {
                        throw new \Exception('failed to put contents into: '. __DIR__ . '/../../..' . $filePath);
                    }
                    return $result;
                }
//                $filePath = $this->minify($filePath, $result);
            case '1':
                $result = '<link rel="stylesheet" type="text/css" href="'.$filePath.'">';
                break;
            case '2':
                $result = '<script src="'.$filePath.'"></script>';
                break;
        }
        return $result;
    }
    
    public function getWritable($path) {
        @touch($path);
        if(!is_writable($path)) {
            $location = __DIR__ . '/../../..';
            $path = $location . $path;
            
            @touch($path);
            if(!is_writable($path)) {
                return false;
            }
        }
        return $path;
    }
    
    public function minify($sourcePath) {
        $fullSourcePath = $this->getWritable($sourcePath);
        if($fullSourcePath===false) return $sourcePath;
        
        $class = '\MatthiasMullie\Minify\\' . (strpos($sourcePath, '.js')!==false ? 'JS' : 'CSS');
        $minifier = new $class($fullSourcePath);

        // save minified file to disk
        $minifiedPath = str_replace(['.css', '.js'], ['.min.css', '.min.js'], $sourcePath);
        $fullMinifiedPath = $this->getWritable($minifiedPath);
        
        
        $css = $minifier->minify($fullMinifiedPath, [
            'preserveComments'=>false,
        ]);
        return $minifiedPath;
    }
    
    public function mergeScripts2Template($template, $scriptList) {
        if(!empty($scriptList)) {
            foreach ($scriptList as $alias => $code) {
                $template = $code . "\n" . $template;
            }
        }
        
        return $template;
    }
    
    public function composeScriptsCode($item, $jsonVarList, $uniqid) {
        !array_key_exists('component_script', $item) && ($item = $this->getComponentScript($item));
        
        $scriptList = [];
        if(!empty($item['component_script'])) {
            $scriptList = \Crush\Collection::transform($item['component_script'], 'alias', ['alias', 'code', 'id', 'file_ext', 'file_ext_precompiled']);
            
            $this->loadModelInstance($this->getAttr('foreignKeys')['component_script']['model']);
            foreach ($scriptList as $scriptAlias => $script) {
                $filename = $this->model['Component_script']->getFileName($script);
                $filepath = $this->model['Component_script']->getScriptsDir().$filename;
                $content = @file_get_contents($filepath);
                
                $jsonVarList['filename'] = $filename;
                $jsonVarList['filepath'] = $filepath;
                $jsonVarList['content'] = $content;
                $jsonVarList['alias'] = $scriptAlias;
                
                $jsonVarKeyList = array_map(function($key) {
                    return '{{'.$key.'}}';
                }, array_keys($jsonVarList));
                $scriptList[$scriptAlias] = str_replace($jsonVarKeyList, array_values($jsonVarList), $script['code']);
            }
        }
        
        return $scriptList;
    }

    protected $dataFeedJsonTemplate = '<script type="text/javascript">var {{json_var}} = {{json}};</script>';

    /**
     * Adds results into json variables for feeders with json_output = 1
     * @param string $template
     * @param type $item
     * @param type $dataFeedResults
     * @param type $uniqid
     * @return string
     */
    public function composeJsonScripts($template, $item, $dataFeedResults, $uniqid) {
        $jsonList = $this->buildJsonDataFeed($item, $dataFeedResults);

        $jsonVarList = [];
        foreach ($jsonList as $alias => $json) {
            $jsonVar = $this->buildJsonVar($alias, $uniqid);
            $jsonTemplate = str_replace(['{{json_var}}', '{{alias}}', '{{uniqid}}', '{{json}}'], [$jsonVar, $alias, $uniqid, $json], $this->dataFeedJsonTemplate);
            $template = $jsonTemplate . $template;
            
            $jsonVarList['_json.'.$alias] = $jsonVar;
        }
        $jsonVarList['uniqid'] = $uniqid;

        return [$template, $jsonVarList];
    }

    /**
     * Adds settings into json variable for scripts
     * @param string $template
     * @param type $settings
     * @param type $uniqid
     * @return string
     */
    public function composeJsonSettings($template, $settings, $uniqid, $vars) {
        $escapedSettings = $settings;
        
        $jsonSettings = json_encode($escapedSettings);
        $jsonVar = $this->buildJsonVar('settings', $uniqid);
        $jsonTemplate = str_replace(['{{json_var}}', '{{json}}'], [$jsonVar, $jsonSettings], $this->dataFeedJsonTemplate);
        
        $template = static::CMS_RenderHTML($jsonTemplate, $vars, false, ['escape']) . $template;
        
        return $template;
    }
    
    /**
     * Creates json var name
     * @param type $alias
     * @param type $uniqid
     * @return type
     */
    public function buildJsonVar($alias, $uniqid) {
        $jsonVarTemplate = 'json_{{alias}}_{{uniqid}}';
        return str_replace(['{{alias}}', '{{uniqid}}'], [$alias, $uniqid], $jsonVarTemplate);
    }

    /**
     * Creates a list of json results just for feeders with json_output = 1
     * @param type $item
     * @param type $dataFeedResults
     * @return type
     */
    public function buildJsonDataFeed($item, $dataFeedResults) {
        $componentDataFeed = \Crush\Collection::transform($item['component_datatype_component'], 'alias');
        $jsonList = array_map(function($item) {
            return json_encode($item);
        }, \Crush\Collection::filter($dataFeedResults, function($value, $key) use ($componentDataFeed) {
                    return !!(int) $componentDataFeed[$key]['json_output'];
                }));

        return $jsonList;
    }

    /**
     * Make able to create component parameters split by "." and have them as a multilevel array into Twig
     * @param type $settings
     * @return array
     */
    public function buildSettings($settings) {
        $builtSettings = $settings;

        if (!empty($settings)) {
            foreach ($settings as $key => $value) {
                if (is_array($key)) {
                    continue;
                }

                if (!empty(strpos($key, '.'))) {
                    $builtSettings = array_replace_recursive($builtSettings, $this->buildSettingStack(explode('.', $key), $value));
                    unset($builtSettings[$key]);
                }
            }
        }

        return $builtSettings;
    }

    /**
     * Creates a multilevel array according to key array
     * @param type $key
     * @param type $value
     * @param type $stack
     * @return array
     */
    public function buildSettingStack($key, $value, $stack = []) {
        $currentKey = array_shift($key);

        if ($currentKey) {
            $stack[$currentKey] = $this->buildSettingStack($key, $value);
        } else {
            return $value;
        }

        return $stack;
    }

    /**
     * Allows settings to access global variables [multilevel]
     * @param type $settings
     * @param type $vars
     * @param type $stack
     * @return type
     */
    public static function renderSettings($settings, $vars, $stack = []) {
        if (!empty($settings)) {
            foreach ($settings as $key => $value) {
                /* allows settings to have access to sets.uniqid and to themselves (be carefull with their order while rendering) */
                $dynVars = array_merge($vars, ['sets' => $stack]);
                if (is_array($value)) {
                    $stack[$key] = self::renderSettings($value, $dynVars);
                } else if (strpos($value, '{{') !== false) {
                    $stack[$key] = static::CMS_RenderHTML('{% spaceless %}'.$value.'{% endspaceless %}', $dynVars);
                } else {
                    $stack[$key] = $value;
                }
            }
        }

        return $stack;
    }

    public function sortSettingsByOrder($componentList) {
        if (!empty($componentList)) {
            foreach ($componentList as $x => $y) {
                $componentList[$x] = $this->sortComponentSettings($y);
            }
        }
        return $componentList;
    }

    protected function prepareRelatedData_component_settings($foreignData, $data, $foreignConfig) {
        if (!empty($foreignData)) {
            foreach ($foreignData as $x => $y) {

                $y['entry_name'] = preg_replace('/\s+/', '', $y['entry_name']);
                $foreignData[$x] = $y;
            }
        }

        return $foreignData;
    }

    public function getEntryListData($type, $config, $pageData = []) {
        $config = explode(',', $config);
        $results = [];
        switch ($type) {
            case 'feed':
                $this->loadModelInstance($this->getAttr('foreignKeys')['component_datatype_component']['model']);
//                $this->loadModelInstance($this->model['Component_datatype_component']->getAttr('foreignKeys')['component_datatype']['model']);
                $sets = array_merge(['random' => '', 'limit' => 999], (array) @$_POST);

                list($model, $method) = $this->getFeedMethod($config[0]);
                $methodPieces = explode('_', $method, 2);
                $feederName = preg_replace('/_model$/','',strtolower(\Crush\Basic::getClassShortName($model)));
                $methodPieces[0] === 'datatype' && count($methodPieces) > 1 && ($feederName .= '_'.$methodPieces[1]);
                
                $data = $model->{$method}([], null, ['_feed' => [$feederName => $sets]]);
                $data = \Crush\Collection::transform($data, '', [$config[1], $config[2]]);

                $results = [
                    'data' => $data,
                    'value' => $config[1],
                    'text' => $config[2]
                ];
                break;
            case 'datatype':
                $this->loadModelInstance($this->getAttr('foreignKeys')['component_datatype_component']['model']);
//                $this->loadModelInstance($this->model['Component_datatype_component']->getAttr('foreignKeys')['component_datatype']['model']);
                $sets = array_merge(['random' => '', 'limit' => 999], (array) @$_REQUEST['sets']);

                list($model, $method) = $this->getFeedMethod($config[0]);
                $methodPieces = explode('_', $method, 2);
                $feederName = preg_replace('/_model$/','',strtolower(\Crush\Basic::getClassShortName($model)));
                $methodPieces[0] === 'datatype' && count($methodPieces) > 1 && ($feederName .= '_'.$methodPieces[1]);
                
                $data = $model->{$method}([], null, ['_feed' => [$feederName => $sets]]);

                $results = $data;
                break;
            case 'dyn':
                list($dyn_ukey, $ukey) = $this->readEntryList($config[0]);
                
                $dynModel = $this->loadModel('\Model\Dynamic');
                $list = $dynModel->getList([$ukey,$dyn_ukey]);
                if(!empty($list[0]) && !empty($list[0]['hasmany'])) {
                    $result = \Crush\Collection::transform($list[0]['hasmany'], '', [$config[1], $config[2]]);
                }
                
                $results = [
                    'data' => $result,
                    'value' => $config[1],
                    'text' => $config[2]
                ];
                break;
            case 'inherit':
                $config[0] = explode('.', $config[0]);
                $results = NULL;
                if (!empty($config[0])) {
                    $results = $pageData;
                    foreach ($config[0] as $key) {
                        if (!array_key_exists($key, $results)) {
                            $results = NULL;
                            break;
                        }
                        $results = $results[$key];
                    }
                }
                break;
            default:
                return false;
        }

        return $results;
    }

}
