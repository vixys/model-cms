<?php

namespace Model\CMS;

class Page {

    use \doctrine\Dashes\ControlRecord,
        \Model\CMS\Component_datatype_behavior {
            \doctrine\Dashes\ControlRecord::create as protected _create;
            \doctrine\Dashes\ControlRecord::update as protected _update;
    //        \doctrine\Dashes\Model::find as protected _find;
            \doctrine\Dashes\ControlRecord::getBy as protected _getBy;
    }

    protected $modelAttrDefaults = [
        'table' => 'page',
        'recursive' => \HBasis\HASMANY,
//    protected $relatedRecursive = \HBasis\HASMANY;
        'foreignKeys' => [
            'page' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'page_id',
                'model' => '\Model\Page'
            ],
            'page_block' => [
                'type' => \HBasis\HASMANY,
                'key' => 'page_id',
                'model' => '\Model\Page_block'
            ],
        ],
        'fieldsFormat' => [
            'page_id' => ':',
            'slug' => ':',
            'old_slug' => ':',
        ],
    ];

    public $actionDefault = ['module'=>'-','class'=>'-', 'action'=>'-'];
    
    public function format_page_id($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        return !empty($data['page_id']) ? $data['page_id'] : null;
    }

    public function prepareRelatedData_page_block($foreignData, $data, $foreignConfig) {
        if (!empty($foreignData)) {
            foreach ($foreignData as $x => $foreignItemData) {
                $foreignData[$x] = $foreignItemData;
            }
        }
        return $foreignData;
    }

    /**
     * Set default params applied on the website
     * @param type $item page
     * @param type $ownerItem category owner
     */
    public function setDefaultParams(&$params, $item, $ownerItem, $data = []) {
        $params['vars'] = [];
        $params['vars']['title'] = (!empty($item['title']) ? $item['title'] : (!empty($params['category_id']) ? @$data['category']['title'] : ''));
        $params['vars']['full_title'] = (!empty($item['title']) ? $item['title'] : (!empty($params['category_id']) ? @$data['category']['title'] : '')) . (!empty($ownerItem['title']) ? ' - ' . $ownerItem['title'] : '');
        $params['vars']['site_title'] = $ownerItem['title'];
        $params['vars']['meta_type'] = 'website';
        $params['vars']['meta_description'] = !empty($item['meta_description']) ? $item['meta_description'] : (!empty($params['category_id']) && !empty($this->data['category']['meta_description']) ? $this->data['category']['meta_description'] : $params['vars']['title']);
        $params['vars']['meta_keywords'] = !empty($item['meta_keywords']) ? $item['meta_keywords'] : (!empty($params['category_id']) && !empty($this->data['category']['meta_keywords']) ? $this->data['category']['meta_keywords'] : '');

        $params['vars']['color'] = !empty($ownerItem['theme_color']) ? $ownerItem['theme_color'] : 'blue';
        $params['vars']['logo'] = $ownerItem['id'];
        $params['vars']['category'] = (array) @$data['category'];
    }

    public function compose($id, $data = [], $html = '', $l = 1) {
        if (empty($id))
            return $html;
        $page = $this
                ->setRowActionRecursive(\HBasis\HASMANY)
                ->setRowActionExecute()->getFullPage($id); // needed to compose a page below pages restricted

        // collect vars only for the page called. inherited pages will receive the same stack
        $vars = $l > 1 ? $data : array_merge($data, $this->getVars($page));
        
        $pageHtml = $this->model['Page_block']->composeAll($page, $vars, $html, $l);
        // compoe recursivamente as paginas
        return $this->compose((int) @$page['page_id'], $vars, $pageHtml, ++$l);
    }

    public function getFullPage($id, $edit = false) {
        $this->relatedRecursive = \HBasis\HASMANY;
        $this->loadModelInstance($this->getAttr('foreignKeys')['page_block']['model']);
        $this->model['Page_block']->foreignKeys['page'] = NULL;

//        $this->loadModelInstance($this->getAttr('foreignKeys')['page']['model']);
//        $this->model['Page']->setRowActionExecute(); // not needed because it wont edit parent page
        
        $page = $this->get($id);
        $page = $this->getPageComponentData($page, $edit ? false : true);
        return $page;
    }

    /**
     * Metodo responsavel por coletar dados a respeito de categorias e componentes 
     * relacionados aos "page_component" incluidos na "page". Visa reduzir o número
     * de queries
     * 
     * @param type $data
     * @return type
     */
    public function getPageComponentData($data, $skipDeactivated = true) {
        $this->loadModelInstance($this->getAttr('foreignKeys')['page_block']['model']);
        $this->loadModelInstance($this->model['Page_block']->getAttr('foreignKeys')['page_component']['model']);
        $this->loadModelInstance($this->model['Page_component']->getAttr('foreignKeys')['page_component_settings']['model']);
        $this->loadModelInstance($this->model['Page_component']->getAttr('foreignKeys')['component']['model']);
        $this->loadModelInstance($this->model['Page_component']->getAttr('foreignKeys')['category']['model']);

        $pageComponentIdList = \Crush\Collection::transform((array) @$data['page_block'], '', ['page_component.[].id'], ['flatten', 'removeEmpty', 'interlace', 'under' => ['flatten', 'removeEmpty']]);
        $categoryIdList = \Crush\Collection::transform((array) @$data['page_block'], '', ['page_component.[].category_id'], ['flatten', 'removeEmpty', 'interlace', 'under' => ['flatten', 'removeEmpty']]);
        $componentIdList = \Crush\Collection::transform((array) @$data['page_block'], '', ['page_component.[].component_id'], ['flatten', 'removeEmpty', 'interlace', 'under' => ['flatten', 'removeEmpty']]);

        $this->model['Page_component_settings']->foreignKeys['page_component'] = NULL;
        $pageComponentSettingsList = !empty($pageComponentIdList) ? \Crush\Collection::transform($this->model['Page_component_settings']->find(['page_component_id' => $pageComponentIdList]), 'page_component_id', [], ['array']) : [];

        $categoryList = !empty($categoryIdList) ? \Crush\Collection::transform($this->model['Category']->find(['id' => $categoryIdList]), 'id') : [];
        $componentList = !empty($componentIdList) ? \Crush\Collection::transform($this->model['Component']->find(['id' => $componentIdList]), 'id') : [];

        foreach ((array) @$data['page_block'] as $pageBlockX => $pageBlock) {
            if (!empty($pageBlock['page_component']))
                foreach ($pageBlock['page_component'] as $pageComponentX => $pageComponent) {
                    if (empty($pageComponent['status']) && $skipDeactivated) {
                        unset($pageBlock['page_component'][$pageComponentX]);
                        continue;
                    }
                    !empty($pageComponentSettingsList[$pageComponent['id']]) && ($pageComponent['page_component_settings'] = $pageComponentSettingsList[$pageComponent['id']]);
                    !empty($pageComponent['category_id']) && $pageComponent['category'] = $categoryList[$pageComponent['category_id']];
                    !empty($pageComponent['component_id']) && $pageComponent['component'] = $componentList[$pageComponent['component_id']];

                    $pageBlock['page_component'][$pageComponentX] = $pageComponent;
                }
            $data['page_block'][$pageBlockX] = $pageBlock;
        }
        return $data;
    }

    public function getVars($page) {
//        $menu = $this->model['Menu']->getTreeList();
        $vars = [];
        $pageData = $this->_filterPageInfo($page)[0];
        
        $vars = array_merge($pageData, ['vars'=>static::CMS_getVars()]);

        return $vars;
    }
    
    public function _filterPageInfo($page) {
        $pageData = array_filter($page, function ($value, $key) {
            return !in_array($key, ['page_block']);
        }, ARRAY_FILTER_USE_BOTH );
        
        empty($pageData['page']) && !empty($pageData['page_id']) && ($pageData['page'] = $this->get($pageData['page_id'], null, -1));
        $pageInheritance = !empty($pageData['page']) && is_array($pageData['page']) ? $this->_filterPageInfo($pageData['page']) : [$pageData,$pageData];
        list($pageData['page'], $pageData['_root']) = $pageInheritance;
//        array_key_exists('page', $pageData) && (list($pageData['page']) = $this->_filterPageInfo($pageData['page']));
        return [$pageData, $pageInheritance[1]];
    }

    public function create($data) {
        $res = $this->_create($data);
        $this->exportRoutes();

        return $res;
    }

    public function update($id, $data) {
        $res = $this->_update($id, $data);
        $this->exportRoutes();

        return $res;
    }

    public $routeTemplate = "<?php\n{% for item in routes %}
            \$route['({{item.route}})'] = '{{item.path}}/$1';\n
            \$route['({{item.route}})/(:any)/(:any)'] = '{{item.path}}/$1/$2/$3';\n
            \$route['({{item.route}})/(:any)/(:any)/(:any)/(:any)'] = '{{item.path}}/$1/$2/$3/$4/$5';\n
            \$route['({{item.route}})/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '{{item.path}}/$1/$2/$3/$4/$5/$6/$7';\n
            {% endfor %} ?>";
    public $routeReservedTemplate = "<?php\n{% for item in routes %}
            \$route['({{item.route}})'] = '{{item.path}}/$1';\n
            \$route['({{item.route}})/(:any)'] = '{{item.path}}/$1/$1/$2';\n
            \$route['({{item.route}})/(:any)/(:any)/(:any)'] = '{{item.path}}/$1/$1/$2/$3/$4';\n
            \$route['({{item.route}})/(:any)/(:any)/(:any)/(:any)/(:any)'] = '{{item.path}}/$1/$1/$2/$3/$4/$5/$6';\n
            {% endfor %} ?>";
    public $reservedKeyList = [
        'categoria' => 'category_id',
        'sl' => 'filter_slug',
        'colunista' => 'filter_publisher',
        'titulo' => 'filter_title',
        'busca' => 'filter_text',
    ];

    /**
     * Dynamic route generator
     */
    public function exportRoutes() {
        $res = $this->setRowActionExecute()->find(['model' => "0"], null, null, null, null, \HBasis\NORELATED);

//        $routes = [];
        $html = '';
        foreach ($res as $item) {
            if (empty($item['slug'])) {
                continue;
            }

            $route = [];
            $route['route'] = $item['slug'];
            $route['path'] = 'page/open';

//            $routes[] = $route;

            $routeTpl = in_array($item['slug'], array_keys($this->reservedKeyList)) ? $this->routeReservedTemplate : $this->routeTemplate;
            $html .= static::CMS_RenderHTML($routeTpl, ['routes' => [$route]]);
        }

        $this->saveRoutesFile('routes/routes.page', $html);
    }

    /**
     * Keeps the number of page views
     */
    public function addView($item) {
        is_array($item) ? ($id = $item['id']) : ($id = $item);

        if (!$this->getData($id)) {
            $item = $this->get($id);
            $item['views'] = (int) $item['views'] + 1;
            $this->update($id, $item);
            $this->addData($id, true);
            return true;
        }

        return false;
    }

    public function getList($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $conditions['status'] = '1';

        return $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

    public function getBy($conditions = array(), $columns = null, $orderby = null, $recursive = null) {
        array_search('status IS NOT NULL', $conditions) === false && ($conditions['status'] = '1');

        $item = $this->_getBy($conditions, $columns, $orderby, $recursive);
        return $item;
    }

    public function datatype($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, !empty($item['alias']) ? $item['alias'] : \Crush\Basic::getClassShortName($this));

        !empty($mySets['page_id']) && ($criteria['page_id'] = $mySets['page_id']);
        !array_key_exists('model', $mySets) && ($mySets['model'] = '0');
        !empty($mySets['model']) && ($criteria['model'] = $mySets['model']);

        $results = ($this->execCommonFind($this, [], $categoryId, $mySets));
        return $results;
    }

}