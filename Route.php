<?php

namespace Model\CMS;

class Route {

    use \doctrine\Dashes\Model,
            \Model\CMS\Component_datatype_behavior,
            \CiTwig\Cms {
        \doctrine\Dashes\Model::create as protected _create;
        \doctrine\Dashes\Model::update as protected _update;
    }

    protected $modelAttrDefaults = [
        'table' => 'route',
    ];

    public function create($data) {
        $res = $this->_create($data);
        $this->exportRoutes();

        return $res;
    }

    public function update($id, $data) {
        $res = $this->_update($id, $data);
        $this->exportRoutes();

        return $res;
    }

    public $routeTemplate = "<?php\n{% for item in routes %}
            \$route['{{item.from}}'] = 'page/open/{{item.to}}';\n
            {% endfor %}";
    public $reservedKeyList = [
        'categoria' => 'category_id',
        'sl' => 'filter_slug',
        'colunista' => 'filter_publisher',
        'titulo' => 'filter_title',
        'busca' => 'filter_text',
    ];

    /**
     * Dynamic route generator
     */
    public function exportRoutes() {
        $res = $this->find(['status' => "1"], null, null, null, null, \HBasis\NORELATED);

        $routes = [];
        $html = '';
        foreach ($res as $item) {
            if (empty($item['from']) || empty($item['to'])) {
                continue;
            }

            $route = [];
            $route['from'] = $item['from'];
            $route['to'] = $item['to'];
            
            foreach($route as $x => $y) $route[$x] = str_replace(['*'], ['(:any)'], $y);
            $routes[] = $route;
        }

        $routeTpl = $this->routeTemplate;
        $html .= static::CMS_RenderHTML($routeTpl, ['routes' => $routes]);
        $this->saveRoutesFile('routes/routes.custom', $html);
    }
    
    public function saveRoutesFile($filepath, $text) {
        $alias = $_SESSION['config']['systemAlias'];
        $filepath .= '.'.$alias;
        file_put_contents(APPPATH . 'config/' . $filepath . '.php', $text);
    }
}
