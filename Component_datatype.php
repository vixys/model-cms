<?php

namespace Model\CMS;

class Component_datatype {

    use \doctrine\Dashes\Model;

    protected $modelAttrDefaults = [
        'table' => 'component_datatype',
//    protected $recursiveRelated = \HBasis\HASMANY;
        'foreignKeys' => [
            'component' => [
                'type' => \HBasis\HASMANY,
                'key' => 'component_datatype_id',
                'model' => '\Model\Component'
            ]
        ],
    ];

}
