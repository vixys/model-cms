<?php

namespace Model\CMS;

class Product_param extends \Model\CMS\Generic_param {

    protected $modelAttrDefaults = [
        'table' => 'product_param',
        'foreignKeys' => [
        ],
        'param_dyn_ukey' => 'product',
    ];
}
