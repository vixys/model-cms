<?php

namespace Model\CMS;

class Publisher_tag extends Generic_bridge_tag {

    protected $modelAttrDefaults = [
        'table' => 'publisher_tag',
        'foreignKeys' => [
            'publisher' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'publisher_id',
                'model' => '\Model\CMS\Publisher'
            ],
            'tag' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'tag_id',
                'model' => '\Model\CMS\Tag'
            ],
        ],
    ];

}
