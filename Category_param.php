<?php

namespace Model\CMS;

class Category_param extends \Model\CMS\Generic_param {

    protected $modelAttrDefaults = [
        'table' => 'category_param',
        'foreignKeys' => [
        ],
        'param_dyn_ukey' => 'category',
    ];
}
