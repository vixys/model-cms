<?php

namespace Model\CMS;

class Page_component_settings {

    use \doctrine\Dashes\Model {
        \doctrine\Dashes\Model::create as protected _create;
    }

    protected $modelAttrDefaults = [
        'table' => 'page_component_settings',
//    protected $recursive = \HBasis\HASMANY;
//    protected $relatedRecursive = \HBasis\HASMANY;
        'foreignKeys' => [
            'page_component' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'page_component_id',
                'model' => '\Model\Page_component'
            ],
            'component_settings' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'component_settings_id',
                'model' => '\Model\Component_settings'
            ],
        ],
    ];

    /**
     * Implemented to reuse settings deativated and avoid keep deleting and creating new records
     * Added a counter to handle multiple values for the same setting
     * @param array $data record
     * @return boolean
     */
    public function create($data) {
        if (empty($data)) {
            return false;
        }

        $data = $this->saveBelongsTo($data);
        $counter = (int) @$data['_counter'];
        if (array_key_exists('_counter', $data)) {
            unset($data['_counter']);
        }

        if (empty($data['id'])) {
            $findData = $data;
            unset($findData['value']);

            $results = $this->findAll($findData, null, null, null, null, \HBasis\NORELATED);
            $pageComponentSetting = count($results) > 0 && !empty($results[$counter]) ? $results[$counter] : [];
            if (!empty($pageComponentSetting)) {
                /* reutiliza settings que foram removidas anteriormente */
                if ((string) $pageComponentSetting[$this->getAttr('deactivate')] === (string)$this->getAttr('deactivateValue')) {
                    $pageComponentSetting[$this->getAttr('deactivate')] = '0';
                }
                $pageComponentSetting['value'] = $data['value'];

                $this->update($pageComponentSetting['id'], $pageComponentSetting);

                return $pageComponentSetting['id'];
            }
        }

        return $this->_create($data);
    }

}
