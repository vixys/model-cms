<?php

namespace Model\CMS;

class Product_tag extends Generic_bridge_tag {

    protected $modelAttrDefaults = [
        'table' => 'product_tag',
        'deactivate' => \DB_FIELD_DELETE,
        'foreignKeys' => [
            'product' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'product_id',
                'model' => '\Model\CMS\Product'
            ],
            'tag' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'tag_id',
                'model' => '\Model\CMS\Tag'
            ],
        ],
    ];

}
