<?php

namespace Model\CMS;

class Post_param extends \Model\CMS\Generic_param {

    protected $modelAttrDefaults = [
        'table' => 'post_param',
        'foreignKeys' => [
        ],
        'param_dyn_ukey' => 'post',
    ];
    
    public function getDynFk($ukey, $paramParentUkey = 'post') {
        return parent::getDynFk($ukey, $paramParentUkey);
    }
    
    public function getDynFkList($paramParentUkey = 'post') {
        return parent::getDynFkList($paramParentUkey);
    }
}
