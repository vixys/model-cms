<?php

namespace Model\CMS;

class Survey {

    use \doctrine\Dashes\Model,
        \Model\CMS\Component_datatype_behavior {
        \doctrine\Dashes\Model::getBy as protected _getBy;
    }

    protected $modelAttrDefaults = [
        'table' => 'survey',
        'recursive' => \HBasis\HASMANY,
        'relatedRecursive' => \HBasis\HASMANY,
        'foreignKeys' => [
            'question' => [
                'type' => \HBasis\HASMANY,
                'key' => 'survey_id',
                'model' => '\Model\CMS\Survey_question'
            ],
            'category' => [
                'type' => \HBasis\BELONGSTO,
                'key' => 'category_id',
                'model' => '\Model\CMS\Category'
            ],
        ],
        'fieldsFormat' => [
            'category_id' => ':',
            'created' => ':',
            'modified' => ':',
            'start' => ':',
            'end' => ':',
        ],
    ];

    const type_multiple = 1;
    const type_unique = 2;

    public function format_category_id($field, $value, $format, $data) {
        if ($value === NULL && !isset($data['category_id']))
            return false;

        if (empty($value)) {
            return NULL;
        }

        return $value;
    }

    public function format_start($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        if (empty($value)) {
            return NULL;
        }
        return implode('-', array_reverse(explode('/', $value)));
    }

    public function format_end($field, $value, $format, $data) {
        if ($value === NULL)
            return false; // variable not used/changed on the proccess

        if (empty($value)) {
            return NULL;
        }
        return implode('-', array_reverse(explode('/', $value)));
    }

    public function getList($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $conditions['start' . ' <='] = date('Y-m-d');
        $conditions[] = '(end IS NULL OR end >= \'' . date('Y-m-d') . '\')';
        $conditions['status'] = '1';

        return $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

    public function getBy($conditions = array(), $columns = null, $orderby = null, $recursive = null) {
        $conditions['start' . ' <='] = date('Y-m-d');
        $conditions[] = '(end IS NULL OR end >= \'' . date('Y-m-d') . '\')';
        $conditions['status'] = '1';

        $item = $this->_getBy($conditions, $columns, $orderby, $recursive);
        return $item;
    }

    public function datatype($item, $categoryId = NULL, $settings = []) {
        $mySets = $this->_getSettings($settings, !empty($item['alias']) ? $item['alias'] : \Crush\Basic::getClassShortName($this));

        $criteria = [];

        empty($mySets['limit']) && ($mySets['limit'] = 1);

        $results = $this->execCommonFind($this, $criteria, $categoryId, $mySets);
//        printf('<pre>%s</pre>', var_export($results, true));die;
        return $results;
    }

}
